package com.ooziesoft.recipeservice.controllers;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ooziesoft.recipeservice.controllers.dto.RecipeDTO;
import com.ooziesoft.recipeservice.converters.RecipeFromDomain;
import com.ooziesoft.recipeservice.converters.RecipeToDomain;
import com.ooziesoft.recipeservice.domain.model.RecipeEntity;
import com.ooziesoft.recipeservice.service.RecipeService;
import com.ooziesoft.recipeservice.service.RecipeServiceRecipeNotFoundException;
import com.ooziesoft.recipeservice.utils.RandomUtils;

/**
 * RecipeControllerWebMvcTest is a set of unit tests for the RecipeController class using the Spring WebMvcTest
 * framework. It tests various endpoints for creating, updating, retrieving, and deleting recipes, as well as error
 * handling scenarios.
 */
@WebMvcTest(RecipeController.class)
@WithMockUser
public class RecipeControllerWebMvcTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RecipeService entityService;

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * Tests the creation of a new recipe when a valid request is made.
     *
     * @throws Exception If there is an error during the test.
     */
    @Test
    public void shouldCreateRecipeWhenValidRequest() throws Exception {

        final String id = RandomUtils.randomId();
        final RecipeDTO dto = RecipeDTO.createRandom().toBuilder().id(null).build();
        final RecipeEntity entityReturned = RecipeToDomain.convertDTO(dto).toBuilder().id(id).createdAt(1l)
                .lastUpdate(2l).build();
        when(entityService.createRecipe(dto)).thenReturn(new RecipeFromDomain().convert(entityReturned));
        mockMvc.perform(post("/api/recipe").contentType(MediaType.APPLICATION_JSON).with(csrf())
                .content(objectMapper.writeValueAsString(dto))).andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(id))).andExpect(jsonPath("$.createdAt", is(1l), Long.class))
                .andExpect(jsonPath("$.lastUpdate", is(2l), Long.class))
                .andExpect(jsonPath("$.name", is(dto.getName())))
                .andExpect(jsonPath("$.vegetarian", is(dto.getVegetarian())))
                .andExpect(jsonPath("$.servings", is(dto.getServings())))
                .andExpect(jsonPath("$.instructions", is(dto.getInstructions())))
                .andExpect(jsonPath("$.ingredients", hasSize(1)))
                .andExpect(jsonPath("$.ingredients[0].name", is(dto.getIngredients().get(0).getName())))
                .andExpect(jsonPath("$.ingredients[0].quantity", is(dto.getIngredients().get(0).getQuantity())))
                .andExpect(jsonPath("$.ingredients[0].unit", is(dto.getIngredients().get(0).getUnit()))).andReturn();

    }

    /**
     * Tests the update of an existing recipe when a valid request is made.
     *
     * @throws Exception If there is an error during the test.
     */
    @Test
    public void shouldUpdateRecipeWhenValidRequest() throws Exception {

        final String id = RandomUtils.randomId();
        final RecipeDTO dto = RecipeDTO.createRandom().toBuilder().id(null).build();
        final RecipeEntity entityReturned = RecipeToDomain.convertDTO(dto).toBuilder().id(id).build();
        when(entityService.getRecipe(id)).thenReturn(RecipeDTO.createRandom());
        when(entityService.updateRecipe(dto.toBuilder().id(id).build()))
                .thenReturn(RecipeFromDomain.convertEntity(entityReturned));
        mockMvc.perform(put("/api/recipe/{id}", id).contentType(MediaType.APPLICATION_JSON).with(csrf())
                .content(objectMapper.writeValueAsString(dto))).andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(id)))
                .andExpect(jsonPath("$.createdAt", is(entityReturned.getCreatedAt()), Long.class))
                .andExpect(jsonPath("$.lastUpdate", is(entityReturned.getLastUpdate()), Long.class))
                .andExpect(jsonPath("$.name", is(dto.getName())))
                .andExpect(jsonPath("$.vegetarian", is(dto.getVegetarian())))
                .andExpect(jsonPath("$.servings", is(dto.getServings())))
                .andExpect(jsonPath("$.instructions", is(dto.getInstructions())))
                .andExpect(jsonPath("$.ingredients", hasSize(1)))
                .andExpect(jsonPath("$.ingredients[0].name", is(dto.getIngredients().get(0).getName())))
                .andExpect(jsonPath("$.ingredients[0].quantity", is(dto.getIngredients().get(0).getQuantity())))
                .andExpect(jsonPath("$.ingredients[0].unit", is(dto.getIngredients().get(0).getUnit()))).andReturn();

    }

    /**
     * Tests the scenario when trying to update a recipe with a nonexistent ID, which should result in an error.
     *
     * @throws Exception If there is an error during the test.
     */
    @Test
    public void shouldReturnErrorWhenUpdateWithNonexistentId() throws Exception {

        final String id = RandomUtils.randomId();
        final RecipeDTO dto = RecipeDTO.createRandom().toBuilder().id(null).build();
        doThrow(RecipeServiceRecipeNotFoundException.class).when(entityService)
                .updateRecipe(dto.toBuilder().id(id).build());
        mockMvc.perform(put("/api/recipe/{id}", id).contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto))).andExpect(status().is4xxClientError()).andReturn();

    }

    /**
     * Tests the deletion of a recipe when a valid ID is provided.
     *
     * @throws Exception If there is an error during the test.
     */
    @Test
    public void shouldDeleteRecipeWhenValidId() throws Exception {

        final String id = RandomUtils.randomId();
        when(entityService.getRecipe(id)).thenReturn(RecipeDTO.createRandom());
        mockMvc.perform(delete("/api/recipe/{id}", id).contentType(MediaType.APPLICATION_JSON).with(csrf()))
                .andExpect(status().isNoContent()).andReturn();

    }

    /**
     * Tests the scenario when trying to delete a recipe with a nonexistent ID, which should result in an error.
     *
     * @throws Exception If there is an error during the test.
     */
    @Test
    public void shouldReturnErrorWhenDeleteWithNonexistentId() throws Exception {

        final String id = "1";
        doThrow(RecipeServiceRecipeNotFoundException.class).when(entityService).deleteRecipe(id);
        mockMvc.perform(delete("/api/recipe/{id}", id).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError()).andReturn();

    }

    /**
     * Tests the retrieval of a recipe when a valid ID is provided.
     *
     * @throws Exception If there is an error during the test.
     */
    @Test
    public void shouldRetrieveRecipeWhenValidId() throws Exception {

        final String id = RandomUtils.randomId();
        final RecipeDTO dto = RecipeDTO.createRandom().toBuilder().id(id).build();
        when(entityService.getRecipe(id)).thenReturn(dto);
        mockMvc.perform(get("/api/recipe/{id}", id).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(id))).andExpect(jsonPath("$.name", is(dto.getName())))
                .andExpect(jsonPath("$.servings", is(dto.getServings())))
                .andExpect(jsonPath("$.vegetarian", is(dto.getVegetarian())))
                .andExpect(jsonPath("$.instructions", is(dto.getInstructions())))
                .andExpect(jsonPath("$.ingredients", hasSize(1)))
                .andExpect(jsonPath("$.ingredients[0].name", is(dto.getIngredients().get(0).getName())))
                .andExpect(jsonPath("$.ingredients[0].quantity", is(dto.getIngredients().get(0).getQuantity())))
                .andExpect(jsonPath("$.ingredients[0].unit", is(dto.getIngredients().get(0).getUnit()))).andReturn();

    }

    /**
     * Tests the scenario when trying to retrieve a recipe with a nonexistent ID, which should result in an error.
     *
     * @throws Exception If there is an error during the test.
     */
    @Test
    public void shouldReturnErrorWhenRetrieveWithNonexistentId() throws Exception {

        final String id = "1";
        doThrow(RecipeServiceRecipeNotFoundException.class).when(entityService).getRecipe(id);
        mockMvc.perform(get("/api/recipe/{id}", id).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError()).andReturn();

    }

    @Test
    public void shouldReturnErrorWhenQueryWithInvalidCriteria() throws Exception {

        mockMvc.perform(get("/api/recipe?searchTerm=123456789012345678901").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.searchTerm", is("size must be between 0 and 20"))).andReturn();
        mockMvc.perform(get("/api/recipe?includedIngredients=1,2,3,4,5,6").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.includedIngredients", is("size must be between 0 and 5"))).andReturn();
        mockMvc.perform(get("/api/recipe?excludedIngredients=1,2,3,4,5,6").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.excludedIngredients", is("size must be between 0 and 5"))).andReturn();

    }

    @Test
    public void shouldRetrieveRecipesWithValidQuery() throws Exception {

        int pageSize = 10;
        String sortBy = "id";
        int totalItems = 20;
        List<RecipeDTO> entities = new ArrayList<>();

        for (int i = 0; i < totalItems; i++) {

            entities.add(RecipeDTO.createRandom().toBuilder().id(String.valueOf(i)).build());

        }

        when(entityService.getAllRecipes(any(), any(), eq(PageRequest.of(0, pageSize, Sort.by(sortBy))))).thenReturn(
                new PageImpl<RecipeDTO>(entities.subList(0, pageSize), Pageable.ofSize(pageSize), totalItems));
        mockMvc.perform(get("/api/recipe?page=0&size=" + pageSize + "&sort=" + sortBy + "")
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].id", is("0"))).andExpect(jsonPath("$.content[1].id", is("1")))
                .andExpect(jsonPath("$.content", hasSize(10))).andReturn();

    }

}
