package com.ooziesoft.recipeservice.converters;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import com.ooziesoft.recipeservice.controllers.dto.IngredientDTO;
import com.ooziesoft.recipeservice.domain.model.Ingredient;
/**
 * Tests for the IngredientToDomain converter.
 */
public class IngredientToDomainTest {

    /**
     * Test to ensure that an IngredientDTO is correctly converted to an Ingredient.
     */
    @Test
    public void shouldConvertCorrectly() {

        final IngredientDTO ingredientDTO = new IngredientDTO("name", "1", "unit");
        final Ingredient Ingredient = new IngredientToDomain().convert(ingredientDTO);
        assertTrue(Ingredient.getName().equals(ingredientDTO.getName()));
        assertTrue(Ingredient.getQuantity().equals(ingredientDTO.getQuantity()));
        assertTrue(Ingredient.getUnit().equals(ingredientDTO.getUnit()));

    }

    /**
     * Test to ensure that a list of IngredientDTOs is correctly converted to a list of IngredientEntities.
     */
    @Test
    public void shouldConvertListCorrectly() {

        final List<IngredientDTO> ingredientDTOList = new ArrayList<>();

        for (int i = 0; i < 10; i++) {

            ingredientDTOList.add(new IngredientDTO("name", String.valueOf(i), "unit"));

        }

        final List<Ingredient> IngredientList = IngredientToDomain.convertDTOList(ingredientDTOList);
        assertTrue(IngredientList.get(0).getName().equals(ingredientDTOList.get(0).getName()));
        assertTrue(IngredientList.get(0).getQuantity().equals(ingredientDTOList.get(0).getQuantity()));
        assertTrue(IngredientList.get(0).getUnit().equals(ingredientDTOList.get(0).getUnit()));
        assertTrue(IngredientList.size() == ingredientDTOList.size());

    }

    /**
     * Test to ensure that converting a null IngredientDTO returns null.
     */
    @Test
    public void shouldReturnNullOnConvert() {

        final Ingredient IngredientList = new IngredientToDomain().convert(null);
        assertTrue(IngredientList == null);

    }

    /**
     * Test to ensure that converting a null list of IngredientDTOs returns null.
     */
    @Test
    public void shouldReturnNullOnConvertPage() {

        final List<Ingredient> IngredientList = IngredientToDomain.convertDTOList(null);
        assertTrue(IngredientList == null);

    }

}
