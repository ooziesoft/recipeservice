package com.ooziesoft.recipeservice.util;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;
import com.ooziesoft.recipeservice.utils.RandomUtils;
/**
 * Test class for RandomUtils utility methods.
 */
public class RandomUtilsTest {

    /**
     * Tests the randomInt method with a specified maximum value. Verifies that all possible integers from 0 to the
     * maximum (inclusive) are generated, ensuring the randomness and range of the method.
     */
    @Test
    public void randomInt_int_max() {

        final Map<Integer, Integer> map = new HashMap<>();
        map.put(0, 0);
        map.put(1, 0);
        map.put(2, 0);
        map.put(3, 0);
        map.put(4, 0);
        map.put(5, 0);
        map.put(6, 0);

        for (int i = 0; i < 10000000; i++) {

            final int rnd = RandomUtils.randomInt(6);
            Assert.isTrue(map.keySet().contains(rnd), "Incorrect results");
            map.put(rnd, 1);

        }

        Assert.isTrue(!map.values().contains(0), "Incorrect results");

    }

    /**
     * Tests the randomInt method with specified minimum and maximum values. Verifies that all possible integers within
     * the given range are generated, ensuring the randomness and correct boundary handling of the method.
     */
    @Test
    public void randomInt_int_min_int_max() {

        final Map<Integer, Integer> map = new HashMap<>();
        map.put(1, 0);
        map.put(2, 0);
        map.put(3, 0);
        map.put(4, 0);
        map.put(5, 0);
        map.put(6, 0);

        for (int i = 0; i < 10000000; i++) {

            final int rnd = RandomUtils.randomInt(1, 6);
            Assert.isTrue(map.keySet().contains(rnd), "Incorrect results");
            map.put(rnd, 1);

        }

        Assert.isTrue(!map.values().contains(0), "Incorrect results");

    }

}
