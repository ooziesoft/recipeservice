package com.ooziesoft.recipeservice.service;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import com.ooziesoft.recipeservice.controllers.dto.RecipeDTO;
import com.ooziesoft.recipeservice.converters.RecipeFromDomain;
import com.ooziesoft.recipeservice.domain.model.RecipeEntity;
import com.ooziesoft.recipeservice.domain.repository.RecipeRepository;
/**
 * Test class for RecipeServiceImpl. This class tests the business logic and handling of CRUD operations implemented in
 * the RecipeServiceImpl. It covers various scenarios including successful operations and expected exceptions.
 */
@SpringBootTest
@WithMockUser
public class RecipeServiceImplTest {

    @Autowired
    private RecipeServiceImpl service;

    @MockBean
    private RecipeRepository repository;

    /**
     * Tests retrieval of a recipe by ID when the recipe exists.
     */
    @Test
    public void shouldRetrieveRecipeWhenItExists() throws RecipeServiceException {

        final RecipeEntity entity = RecipeEntity.createRandom().toBuilder().id("1").build();
        when(repository.findById("1")).thenReturn(Optional.of(entity));
        final RecipeDTO dto = service.getRecipe("1");
        assertTrue(dto.getId().equals(entity.getId()));

    }

    /**
     * Tests the behavior when trying to retrieve a non-existent recipe.
     */
    @Test
    public void shouldThrowExceptionWhenRecipeDoesNotExist() {

        when(repository.findById("1")).thenReturn(Optional.empty());
        assertThrows(RecipeServiceRecipeNotFoundException.class, () -> {

            service.getRecipe("1");

        });

    }

    /**
     * Tests updating a recipe successfully.
     */
    @Test
    public void shouldUpdateRecipeSuccessfully() throws RecipeServiceException {

        final RecipeEntity entity = RecipeEntity.createRandom().toBuilder().id("1").lastUpdate(null).createdAt(null)
                .build();
        when(repository.findById("1")).thenReturn(Optional.of(entity.toBuilder().createdAt(100000l).build()));
        final RecipeDTO dto = service.updateRecipe(RecipeFromDomain.convertEntity(entity));
        assertTrue(dto.getId().equals(entity.getId()));
        assertTrue(dto.getId() != null);
        assertTrue(dto.getCreatedAt() == 100000l);
        assertTrue(dto.getLastUpdate() > System.currentTimeMillis() - 10000);

    }

    /**
     * Tests the behavior when trying to update a non-existent recipe.
     */
    @Test
    public void shouldThrowExceptionWhenUpdatingNonExistentRecipe() {

        final RecipeEntity entity = RecipeEntity.createRandom().toBuilder().id("1").lastUpdate(null).createdAt(null)
                .build();
        when(repository.findById("1")).thenReturn(Optional.empty());
        assertThrows(RecipeServiceRecipeNotFoundException.class, () -> {

            service.updateRecipe(RecipeFromDomain.convertEntity(entity));

        });

    }

    /**
     * Tests successful deletion of an existing recipe.
     */
    @Test
    public void shouldDeleteRecipeWhenItExists() throws RecipeServiceException {

        when(repository.existsById("1")).thenReturn(true);
        service.deleteRecipe("1");

    }

    /**
     * Tests the behavior when trying to delete a non-existent recipe.
     */
    @Test
    public void shouldThrowExceptionWhenDeletingNonExistentRecipe() {

        when(repository.existsById("1")).thenReturn(false);
        assertThrows(RecipeServiceRecipeNotFoundException.class, () -> {

            service.deleteRecipe("1");

        });

    }

    /**
     * Tests creating a new recipe successfully.
     */
    @Test
    public void shouldCreateNewRecipeSuccessfully() throws RecipeServiceException {

        final RecipeDTO entity = RecipeDTO.createRandom().toBuilder().id(null).createdAt(null).lastUpdate(null).build();
        final RecipeDTO dto = service.createRecipe(entity);
        assertTrue(dto.getId() != null);
        assertTrue(dto.getCreatedAt() > System.currentTimeMillis() - 10000);
        assertTrue(dto.getLastUpdate() > System.currentTimeMillis() - 10000);

    }

    /**
     * Tests the behavior when creating a recipe with an existing ID.
     */
    @Test
    public void shouldThrowExceptionWhenCreatingRecipeWithExistingId() {

        final RecipeDTO entity = RecipeDTO.createRandom().toBuilder().id("1").build();
        assertThrows(RecipeServiceBadRequestException.class, () -> {

            service.createRecipe(entity);

        });

    }

    /**
     * Tests the behavior when creating a recipe with a pre-set creation timestamp.
     */
    @Test
    public void shouldThrowExceptionWhenCreatingRecipeWithPreSetCreatedAt() {

        final RecipeDTO entity = RecipeDTO.createRandom().toBuilder().createdAt(1l).build();
        assertThrows(RecipeServiceBadRequestException.class, () -> {

            service.createRecipe(entity);

        });

    }

    /**
     * Tests the behavior when creating a recipe with a pre-set update timestamp.
     */
    @Test
    public void shouldThrowExceptionWhenCreatingRecipeWithPreSetUpdatedAt() {

        final RecipeDTO entity = RecipeDTO.createRandom().toBuilder().lastUpdate(1l).build();
        assertThrows(RecipeServiceBadRequestException.class, () -> {

            service.createRecipe(entity);

        });

    }

    /**
     * Tests the behavior when updating a recipe without an ID.
     */
    @Test
    public void shouldThrowExceptionWhenUpdatingRecipeWithoutId() {

        final RecipeDTO entity = RecipeDTO.createRandom().toBuilder().id(null).build();
        assertThrows(RecipeServiceBadRequestException.class, () -> {

            service.updateRecipe(entity);

        });

    }

    /**
     * Tests the behavior when updating a recipe with a pre-set creation timestamp.
     */
    @Test
    public void shouldThrowExceptionWhenUpdatingRecipeWithPreSetCreatedAt() {

        final RecipeDTO entity = RecipeDTO.createRandom().toBuilder().createdAt(1l).build();
        assertThrows(RecipeServiceBadRequestException.class, () -> {

            service.updateRecipe(entity);

        });

    }

    /**
     * Tests the behavior when updating a recipe with a pre-set update timestamp.
     */
    @Test
    public void shouldThrowExceptionWhenUpdatingRecipeWithPreSetUpdatedAt() {

        final RecipeDTO entity = RecipeDTO.createRandom().toBuilder().lastUpdate(1l).build();
        assertThrows(RecipeServiceBadRequestException.class, () -> {

            service.updateRecipe(entity);

        });

    }

}
