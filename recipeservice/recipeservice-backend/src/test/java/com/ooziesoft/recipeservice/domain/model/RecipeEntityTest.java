package com.ooziesoft.recipeservice.domain.model;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.io.Serializable;
import org.junit.jupiter.api.Test;
/**
 * Tests for the RecipeEntity class.
 */
public class RecipeEntityTest implements Serializable {

    /**
     * Test to ensure that all fields of RecipeEntity are initialized to null.
     */
    @Test
    void shouldInitializeNullFields() {

        final RecipeEntity recipeEntity = new RecipeEntity();
        assertTrue(recipeEntity.getCreatedAt() == null);
        assertTrue(recipeEntity.getId() == null);
        assertTrue(recipeEntity.getIngredients() == null);
        assertTrue(recipeEntity.getInstructions() == null);
        assertTrue(recipeEntity.getLastUpdate() == null);
        assertTrue(recipeEntity.getName() == null);
        assertTrue(recipeEntity.getServings() == null);
        assertTrue(recipeEntity.getVegetarian() == null);
        assertTrue(recipeEntity.getUsername() == null);

    }

    /**
     * Test to ensure that a RecipeEntity can be correctly built from a builder with given values.
     */
    @Test
    void shouldBuildCorrectly() {

        final RecipeEntity recipeOriginal = RecipeEntity.createRandom();
        final RecipeEntity recipeEntity = RecipeEntity.builder().createdAt(recipeOriginal.getCreatedAt())
                .id(recipeOriginal.getId()).ingredientEntities(recipeOriginal.getIngredients())
                .instructions(recipeOriginal.getInstructions()).lastUpdate(recipeOriginal.getLastUpdate())
                .name(recipeOriginal.getName()).servings(recipeOriginal.getServings())
                .username(recipeOriginal.getUsername()).vegetarian(recipeOriginal.getVegetarian()).build();
        assertTrue(recipeEntity.getCreatedAt().equals(recipeOriginal.getCreatedAt()));
        assertTrue(recipeEntity.getId().equals(recipeOriginal.getId()));
        assertTrue(recipeEntity.getIngredients().equals(recipeOriginal.getIngredients()));
        assertTrue(recipeEntity.getInstructions().equals(recipeOriginal.getInstructions()));
        assertTrue(recipeEntity.getLastUpdate().equals(recipeOriginal.getLastUpdate()));
        assertTrue(recipeEntity.getName().equals(recipeOriginal.getName()));
        assertTrue(recipeEntity.getServings().equals(recipeOriginal.getServings()));
        assertTrue(recipeEntity.getVegetarian().equals(recipeOriginal.getVegetarian()));
        assertTrue(recipeEntity.getUsername().equals(recipeOriginal.getUsername()));

    }

    /**
     * Test to verify that a RecipeEntity can be correctly built from another RecipeEntity instance and converted back
     * to builder.
     */
    @Test
    void shouldBuildFromRecipeEntityAndBackToBuilderCorrectly() {

        final RecipeEntity recipeOriginal = RecipeEntity.createRandom();
        final RecipeEntity recipeEntity = RecipeEntity.builder().createdAt(recipeOriginal.getCreatedAt())
                .id(recipeOriginal.getId()).ingredientEntities(recipeOriginal.getIngredients())
                .instructions(recipeOriginal.getInstructions()).lastUpdate(recipeOriginal.getLastUpdate())
                .name(recipeOriginal.getName()).servings(recipeOriginal.getServings())
                .vegetarian(recipeOriginal.getVegetarian()).username(recipeOriginal.getUsername()).build();
        final RecipeEntity builtRecipe = recipeEntity.toBuilder().build();
        assertTrue(recipeEntity != builtRecipe);
        assertTrue(recipeEntity.getCreatedAt().equals(recipeOriginal.getCreatedAt()));
        assertTrue(recipeEntity.getId().equals(recipeOriginal.getId()));
        assertTrue(recipeEntity.getIngredients().equals(recipeOriginal.getIngredients()));
        assertTrue(recipeEntity.getInstructions().equals(recipeOriginal.getInstructions()));
        assertTrue(recipeEntity.getLastUpdate().equals(recipeOriginal.getLastUpdate()));
        assertTrue(recipeEntity.getName().equals(recipeOriginal.getName()));
        assertTrue(recipeEntity.getServings().equals(recipeOriginal.getServings()));
        assertTrue(recipeEntity.getVegetarian().equals(recipeOriginal.getVegetarian()));
        assertTrue(recipeEntity.getUsername().equals(recipeOriginal.getUsername()));

    }

}
