package com.ooziesoft.recipeservice.converters;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import com.ooziesoft.recipeservice.controllers.dto.RecipeDTO;
import com.ooziesoft.recipeservice.domain.model.RecipeEntity;
/**
 * Tests for the RecipeToDomain converter.
 */
public class RecipeToDomainTest {

    /**
     * Test to ensure that a RecipeDTO is correctly converted to a RecipeEntity.
     */
    @Test
    public void shouldConvertCorrectly() {

        final RecipeDTO recipeDTO = RecipeDTO.createRandom();
        final RecipeEntity recipeEntity = new RecipeToDomain().convert(recipeDTO);
        assertTrue(recipeEntity.getCreatedAt().equals(recipeDTO.getCreatedAt()));
        assertTrue(recipeEntity.getId().equals(recipeDTO.getId()));
        assertTrue(recipeEntity.getIngredients().size() == (recipeDTO.getIngredients().size()));
        assertTrue(recipeEntity.getInstructions().equals(recipeDTO.getInstructions()));
        assertTrue(recipeEntity.getLastUpdate().equals(recipeDTO.getLastUpdate()));
        assertTrue(recipeEntity.getName().equals(recipeDTO.getName()));
        assertTrue(recipeEntity.getServings().equals(recipeDTO.getServings()));
        assertTrue(recipeEntity.getVegetarian().equals(recipeDTO.getVegetarian()));
        assertTrue(recipeEntity.getUsername().equals(recipeDTO.getUsername()));

    }

    /**
     * Test to ensure that a list of RecipeDTOs is correctly converted to a list of RecipeEntities.
     */
    @Test
    public void shouldConvertPageCorrectly() {

        final List<RecipeDTO> recipeDTOList = new ArrayList<>();

        for (int i = 0; i < 10; i++) {

            recipeDTOList.add(RecipeDTO.createRandom());

        }

        final List<RecipeEntity> recipeEntity = RecipeToDomain.convertDTOList(recipeDTOList);
        assertTrue(recipeEntity.get(0).getCreatedAt().equals(recipeDTOList.get(0).getCreatedAt()));
        assertTrue(recipeEntity.get(0).getId().equals(recipeDTOList.get(0).getId()));
        assertTrue(recipeEntity.get(0).getIngredients().size() == (recipeDTOList.get(0).getIngredients().size()));
        assertTrue(recipeEntity.get(0).getInstructions().equals(recipeDTOList.get(0).getInstructions()));
        assertTrue(recipeEntity.get(0).getLastUpdate().equals(recipeDTOList.get(0).getLastUpdate()));
        assertTrue(recipeEntity.get(0).getName().equals(recipeDTOList.get(0).getName()));
        assertTrue(recipeEntity.get(0).getServings().equals(recipeDTOList.get(0).getServings()));
        assertTrue(recipeEntity.get(0).getVegetarian().equals(recipeDTOList.get(0).getVegetarian()));
        assertTrue(recipeEntity.get(0).getUsername().equals(recipeDTOList.get(0).getUsername()));
        assertTrue(recipeEntity.size() == recipeDTOList.size());

    }

    /**
     * Test to ensure that converting a null RecipeDTO returns null.
     */
    @Test
    public void shouldReturnNullOnConvert() {

        final RecipeEntity pageRecipeEntity = new RecipeToDomain().convert(null);
        assertTrue(pageRecipeEntity == null);

    }

    /**
     * Test to ensure that converting a null list of RecipeDTOs returns null.
     */
    @Test
    public void shouldReturnNullOnConvertPage() {

        final List<RecipeEntity> pageRecipeEntity = RecipeToDomain.convertDTOList(null);
        assertTrue(pageRecipeEntity == null);

    }

}
