package com.ooziesoft.recipeservice.integration;
import static org.hamcrest.Matchers.any;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ooziesoft.recipeservice.controllers.dto.RecipeDTO;
import com.ooziesoft.recipeservice.converters.RecipeFromDomain;
import com.ooziesoft.recipeservice.domain.model.RecipeEntity;
import com.ooziesoft.recipeservice.domain.repository.RecipeRepository;
import com.ooziesoft.recipeservice.service.RecipeServiceImpl;
/**
 * Integration tests for the Recipe service. Tests CRUD operations and query functionalities with different criteria.
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser
public class RecipeIT {

    private final Logger logger = LoggerFactory.getLogger(RecipeServiceImpl.class);

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private RecipeRepository recipeRepository;

    @Autowired
    private ObjectMapper objectMapper;

    private RecipeEntity[] testEntities;

    /**
     * Initializes the test environment before each test by creating test entities.
     */
    @BeforeEach
    public void init() {

        try {

            recipeRepository.deleteAll();
            createTestEntities(5);

        } catch (final Exception e) {

            logger.error(e.toString());

        }

    }

    /**
     * Verifies that searching by instructions returns correct results.
     */
    @Test
    public void shouldReturnOneEntityWhenSearchByInstructions() throws Exception {

        recipeRepository.deleteAll();
        testEntities = new RecipeEntity[1];
        testEntities[0] = RecipeEntity.createRandom();
        recipeRepository.save(testEntities[0]);
        mockMvc.perform(get("/api/recipe?searchTerm=instructions").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(jsonPath("$.content", hasSize(1))).andReturn();
        mockMvc.perform(get("/api/recipe?searchTerm=dummy").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(jsonPath("$.content", hasSize(0))).andReturn();

    }

    /**
     * Tests the query functionality for included ingredients.
     */
    @Test
    public void shouldReturnOneEntityWhenIncludedIngredientsMatch() throws Exception {

        recipeRepository.deleteAll();
        testEntities = new RecipeEntity[1];
        testEntities[0] = RecipeEntity.createRandom();
        recipeRepository.save(testEntities[0]);
        mockMvc.perform(get("/api/recipe?includedIngredients=ingredient1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(jsonPath("$.content", hasSize(1))).andReturn();
        mockMvc.perform(get("/api/recipe?includedIngredients=dummy").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(jsonPath("$.content", hasSize(0))).andReturn();

    }

    /**
     * Tests the query functionality for excluded ingredients.
     */
    @Test
    public void shouldReturnNoEntitiesWhenExcludedIngredientsMatch() throws Exception {

        recipeRepository.deleteAll();
        testEntities = new RecipeEntity[1];
        testEntities[0] = RecipeEntity.createRandom();
        recipeRepository.save(testEntities[0]);
        mockMvc.perform(get("/api/recipe?excludedIngredients=ingredient1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(jsonPath("$.content", hasSize(0))).andReturn();
        mockMvc.perform(get("/api/recipe?excludedIngredients=dummy").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(jsonPath("$.content", hasSize(1))).andReturn();

    }

    /**
     * Tests the query functionality for servings criteria.
     */
    @Test
    public void shouldReturnEntitiesMatchingServingsCriteria() throws Exception {

        recipeRepository.deleteAll();
        testEntities = new RecipeEntity[1];
        testEntities[0] = RecipeEntity.createRandom().toBuilder().servings(5).build();
        recipeRepository.save(testEntities[0]);
        mockMvc.perform(get("/api/recipe?servings=4").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(jsonPath("$.content", hasSize(1))).andReturn();
        mockMvc.perform(get("/api/recipe?servings=6").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(jsonPath("$.content", hasSize(0))).andReturn();

    }

    /**
     * Tests the query functionality for vegetarian criteria.
     */
    @Test
    public void shouldReturnEntitiesMatchingVegetarianCriteria() throws Exception {

        recipeRepository.deleteAll();
        testEntities = new RecipeEntity[1];
        testEntities[0] = RecipeEntity.createRandom().toBuilder().vegetarian(true).build();
        recipeRepository.save(testEntities[0]);
        mockMvc.perform(get("/api/recipe?vegetarian=true").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(jsonPath("$.content", hasSize(1))).andReturn();
        mockMvc.perform(get("/api/recipe?vegetarian=1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(jsonPath("$.content", hasSize(1))).andReturn();
        mockMvc.perform(get("/api/recipe?vegetarian=false").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(jsonPath("$.content", hasSize(0))).andReturn();
        mockMvc.perform(get("/api/recipe?vegetarian=0").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(jsonPath("$.content", hasSize(0))).andReturn();

    }

    /**
     * Tests the query functionality with combined criteria.
     */
    @Test
    public void shouldReturnEntitiesMatchingCombinedCriteria() throws Exception {

        recipeRepository.deleteAll();
        testEntities = new RecipeEntity[1];
        testEntities[0] = RecipeEntity.createRandom().toBuilder().vegetarian(true).servings(5).build();
        recipeRepository.save(testEntities[0]);
        mockMvc.perform(get(
                "/api/recipe?vegetarian=true&servings=4&excludedIngredients=dummy&includedIngredients=ingredient1&searchTerm=instructions")
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(1))).andReturn();
        mockMvc.perform(get(
                "/api/recipe?vegetarian=false&servings=4&excludedIngredients=dummy&includedIngredients=ingredient1&searchTerm=instructions")
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(0))).andReturn();
        mockMvc.perform(get(
                "/api/recipe?vegetarian=true&servings=6&excludedIngredients=dummy&includedIngredients=ingredient1&searchTerm=instructions")
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(0))).andReturn();
        mockMvc.perform(get(
                "/api/recipe?vegetarian=true&servings=4&excludedIngredients=ingredient1&includedIngredients=ingredient1&searchTerm=instructions")
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(0))).andReturn();
        mockMvc.perform(get(
                "/api/recipe?vegetarian=true&servings=4&excludedIngredients=dummy&includedIngredients=dummy&searchTerm=instructions")
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(0))).andReturn();
        mockMvc.perform(get(
                "/api/recipe?vegetarian=true&servings=4&excludedIngredients=dummy&includedIngredients=ingredient1&searchTerm=dummy")
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(0))).andReturn();

    }

    /**
     * Tests retrieving all entities.
     */
    @Test
    public void shouldReturnAllEntitiesWhenGetAllIsCalled() throws Exception {

        mockMvc.perform(get("/api/recipe").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(5)))
                .andExpect(jsonPath("$.content[0].id", is(testEntities[0].getId())))
                .andExpect(jsonPath("$.content[0].createdAt", is(testEntities[0].getCreatedAt()), Long.class))
                .andExpect(jsonPath("$.content[0].lastUpdate", is(testEntities[0].getLastUpdate()), Long.class))
                .andExpect(jsonPath("$.content[0].name", is(testEntities[0].getName())))
                .andExpect(jsonPath("$.content[0].vegetarian", is(testEntities[0].getVegetarian())))
                .andExpect(jsonPath("$.content[0].servings", is(testEntities[0].getServings())))
                .andExpect(jsonPath("$.content[0].instructions", is(testEntities[0].getInstructions())))
                .andExpect(jsonPath("$.content[0].ingredients", hasSize(1)))
                .andExpect(jsonPath("$.content[0].ingredients[0].name",
                        is(testEntities[0].getIngredients().get(0).getName())))
                .andExpect(jsonPath("$.content[0].ingredients[0].quantity",
                        is(testEntities[0].getIngredients().get(0).getQuantity())))
                .andExpect(jsonPath("$.content[0].ingredients[0].unit",
                        is(testEntities[0].getIngredients().get(0).getUnit())))
                .andReturn();

    }

    /**
     * Tests retrieving a single entity by ID.
     */
    @Test
    public void shouldReturnSingleEntityWhenValidIdIsProvided() throws Exception {

        mockMvc.perform(get("/api/recipe/{id}", testEntities[0].getId()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(jsonPath("$.id", is(testEntities[0].getId())))
                .andExpect(jsonPath("$.createdAt", is(testEntities[0].getCreatedAt()), Long.class))
                .andExpect(jsonPath("$.lastUpdate", is(testEntities[0].getLastUpdate()), Long.class))
                .andExpect(jsonPath("$.name", is(testEntities[0].getName())))
                .andExpect(jsonPath("$.vegetarian", is(testEntities[0].getVegetarian())))
                .andExpect(jsonPath("$.servings", is(testEntities[0].getServings())))
                .andExpect(jsonPath("$.instructions", is(testEntities[0].getInstructions())))
                .andExpect(jsonPath("$.ingredients", hasSize(1)))
                .andExpect(jsonPath("$.ingredients[0].name", is(testEntities[0].getIngredients().get(0).getName())))
                .andExpect(jsonPath("$.ingredients[0].quantity",
                        is(testEntities[0].getIngredients().get(0).getQuantity())))
                .andExpect(jsonPath("$.ingredients[0].unit", is(testEntities[0].getIngredients().get(0).getUnit())))
                .andReturn();

    }

    /**
     * Verifies the behavior when a GET request is made with an invalid ID.
     */
    @Test
    public void shouldReturnClientErrorWhenGetInvalidId() throws Exception {

        mockMvc.perform(get("/api/recipe/{id}", 0).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError()).andReturn();

    }

    /**
     * Tests creating a new entity.
     */
    @Test
    public void shouldAddNewEntityWhenCreateCalled() throws Exception {

        final RecipeDTO dto = RecipeDTO.createRandom().toBuilder().id(null).createdAt(null).lastUpdate(null).build();
        mockMvc.perform(post("/api/recipe").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto))).andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", not(dto.getId())))
                .andExpect(jsonPath("$.createdAt", is(any(Long.class)), Long.class))
                .andExpect(jsonPath("$.lastUpdate", is(any(Long.class)), Long.class))
                .andExpect(jsonPath("$.name", is(dto.getName())))
                .andExpect(jsonPath("$.vegetarian", is(dto.getVegetarian())))
                .andExpect(jsonPath("$.servings", is(dto.getServings())))
                .andExpect(jsonPath("$.instructions", is(dto.getInstructions())))
                .andExpect(jsonPath("$.ingredients", hasSize(1)))
                .andExpect(jsonPath("$.ingredients[0].name", is(dto.getIngredients().get(0).getName())))
                .andExpect(jsonPath("$.ingredients[0].quantity", is(dto.getIngredients().get(0).getQuantity())))
                .andExpect(jsonPath("$.ingredients[0].unit", is(dto.getIngredients().get(0).getUnit()))).andReturn();
        mockMvc.perform(get("/api/recipe").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(6))).andReturn();

    }

    /**
     * Tests updating an existing entity.
     */
    @Test
    public void shouldUpdateEntityCorrectlyWhenValidUpdateRequest() throws Exception {

        final RecipeDTO dto = RecipeFromDomain.convertEntity(testEntities[0]).toBuilder().id(null).createdAt(null)
                .lastUpdate(null).build();
        mockMvc.perform(put("/api/recipe/{id}", testEntities[0].getId()).contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto))).andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(testEntities[0].getId())))
                .andExpect(jsonPath("$.createdAt", is(testEntities[0].getCreatedAt()), Long.class))
                .andExpect(jsonPath("$.lastUpdate", greaterThan(testEntities[0].getLastUpdate()), Long.class))
                .andExpect(jsonPath("$.name", is(dto.getName())))
                .andExpect(jsonPath("$.vegetarian", is(dto.getVegetarian())))
                .andExpect(jsonPath("$.servings", is(dto.getServings())))
                .andExpect(jsonPath("$.instructions", is(dto.getInstructions())))
                .andExpect(jsonPath("$.ingredients", hasSize(1)))
                .andExpect(jsonPath("$.ingredients[0].name", is(dto.getIngredients().get(0).getName())))
                .andExpect(jsonPath("$.ingredients[0].quantity", is(dto.getIngredients().get(0).getQuantity())))
                .andExpect(jsonPath("$.ingredients[0].unit", is(dto.getIngredients().get(0).getUnit()))).andReturn();

    }

    /**
     * Verifies the behavior when an UPDATE request is made for a non-existent entity.
     */
    @Test
    public void shouldReturnClientErrorWhenUpdateNonExistentId() throws Exception {

        final RecipeDTO dto = RecipeDTO.createRandom();
        mockMvc.perform(put("/api/recipe/{id}", 0).contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto))).andExpect(status().is4xxClientError()).andReturn();

    }

    /**
     * Verifies the behavior when a DELETE request is made for a non-existent entity.
     */
    @Test
    public void shouldReturnClientErrorWhenDeleteNonExistentId() throws Exception {

        mockMvc.perform(delete("/api/recipe/{id}", 0).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError()).andReturn();

    }

    /**
     * Tests deleting an entity and verifies the count of entities after deletion.
     */
    @Test
    public void shouldReduceEntityCountWhenDeleteCalled() throws Exception {

        mockMvc.perform(delete("/api/recipe/{id}", testEntities[0].getId()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent()).andReturn();
        mockMvc.perform(get("/api/recipe").contentType(MediaType.APPLICATION_JSON).with(csrf()))
                .andExpect(status().isOk()).andExpect(jsonPath("$.content", hasSize(4)))
                .andExpect(jsonPath("$.content[0].id", is(testEntities[1].getId()))).andReturn();

    }

    /**
     * Helper method to create a specific number of test entities.
     *
     * @param count The number of entities to create.
     */
    private void createTestEntities(final int count) {

        testEntities = new RecipeEntity[count];

        for (int i = 0; i < count; i++) {

            testEntities[i] = RecipeEntity.createRandom();
            recipeRepository.save(testEntities[i]);

        }

        final List<RecipeEntity> all = recipeRepository.findAll();
        System.out.println("size=" + all.size());

    }

}
