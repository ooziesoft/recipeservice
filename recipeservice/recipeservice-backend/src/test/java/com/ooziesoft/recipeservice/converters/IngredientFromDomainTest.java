package com.ooziesoft.recipeservice.converters;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import com.ooziesoft.recipeservice.controllers.dto.IngredientDTO;
import com.ooziesoft.recipeservice.domain.model.Ingredient;
/**
 * Tests for the IngredientFromDomain converter.
 */
public class IngredientFromDomainTest {

    /**
     * Test to ensure that an Ingredient is correctly converted to an IngredientDTO.
     */
    @Test
    public void shouldConvertCorrectly() {

        final Ingredient ingredientOriginal = new Ingredient("name", "1", "unit");
        final IngredientDTO ingredientDTO = new IngredientFromDomain().convert(ingredientOriginal);
        assertTrue(ingredientDTO.getName().equals(ingredientOriginal.getName()));
        assertTrue(ingredientDTO.getQuantity().equals(ingredientOriginal.getQuantity()));
        assertTrue(ingredientDTO.getUnit().equals(ingredientOriginal.getUnit()));

    }

    /**
     * Test to ensure that a list of IngredientEntities is correctly converted to a list of IngredientDTOs.
     */
    @Test
    public void shouldConvertListCorrectly() {

        final List<Ingredient> IngredientList = new ArrayList<>();

        for (int i = 0; i < 10; i++) {

            IngredientList.add(new Ingredient("name", String.valueOf(i), "unit"));

        }

        final List<IngredientDTO> ingredientDTOList = IngredientFromDomain.convertEntityList(IngredientList);
        assertTrue(ingredientDTOList.get(0).getName().equals(IngredientList.get(0).getName()));
        assertTrue(ingredientDTOList.get(0).getQuantity().equals(IngredientList.get(0).getQuantity()));
        assertTrue(ingredientDTOList.get(0).getUnit().equals(IngredientList.get(0).getUnit()));
        assertTrue(ingredientDTOList.size() == IngredientList.size());

    }

    /**
     * Test to ensure that converting a null Ingredient returns null.
     */
    @Test
    public void shouldReturnNullOnConvert() {

        final IngredientDTO ingredientDTO = new IngredientFromDomain().convert(null);
        assertTrue(ingredientDTO == null);

    }

    /**
     * Test to ensure that converting a null list of IngredientEntities returns null.
     */
    @Test
    public void shouldReturnNullOnConvertPage() {

        final List<IngredientDTO> ingredientDTOList = IngredientFromDomain.convertEntityList(null);
        assertTrue(ingredientDTOList == null);

    }

}
