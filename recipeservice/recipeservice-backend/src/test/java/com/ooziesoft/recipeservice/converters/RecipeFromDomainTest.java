package com.ooziesoft.recipeservice.converters;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import com.ooziesoft.recipeservice.controllers.dto.RecipeDTO;
import com.ooziesoft.recipeservice.domain.model.RecipeEntity;
/**
 * Tests for the RecipeFromDomain converter.
 */
public class RecipeFromDomainTest {

    /**
     * Test to ensure that a RecipeEntity is correctly converted to a RecipeDTO.
     */
    @Test
    public void shouldConvertCorrectly() {

        final RecipeEntity recipeOriginal = RecipeEntity.createRandom();
        final RecipeDTO recipeDTO = new RecipeFromDomain().convert(recipeOriginal);
        assertTrue(recipeDTO.getCreatedAt().equals(recipeOriginal.getCreatedAt()));
        assertTrue(recipeDTO.getId().equals(recipeOriginal.getId()));
        assertTrue(recipeDTO.getIngredients().size() == (recipeOriginal.getIngredients().size()));
        assertTrue(recipeDTO.getInstructions().equals(recipeOriginal.getInstructions()));
        assertTrue(recipeDTO.getLastUpdate().equals(recipeOriginal.getLastUpdate()));
        assertTrue(recipeDTO.getName().equals(recipeOriginal.getName()));
        assertTrue(recipeDTO.getServings().equals(recipeOriginal.getServings()));
        assertTrue(recipeDTO.getVegetarian().equals(recipeOriginal.getVegetarian()));
        assertTrue(recipeDTO.getUsername().equals(recipeOriginal.getUsername()));

    }

    /**
     * Test to ensure that a Page of RecipeEntities is correctly converted to a Page of RecipeDTOs.
     */
    @Test
    public void shouldConvertPageCorrectly() {

        final List<RecipeEntity> recipeEntityList = new ArrayList<>();

        for (int i = 0; i < 20; i++) {

            recipeEntityList.add(RecipeEntity.createRandom());

        }

        final PageImpl<RecipeEntity> page = new PageImpl<>(recipeEntityList, Pageable.ofSize(10), 20);
        final Page<RecipeDTO> recipeDTO = RecipeFromDomain.convertEntityList(page);
        assertTrue(recipeDTO.getContent().get(0).getCreatedAt().equals(recipeEntityList.get(0).getCreatedAt()));
        assertTrue(recipeDTO.getContent().get(0).getId().equals(recipeEntityList.get(0).getId()));
        assertTrue(recipeDTO.getContent().get(0).getIngredients()
                .size() == (recipeEntityList.get(0).getIngredients().size()));
        assertTrue(recipeDTO.getContent().get(0).getInstructions().equals(recipeEntityList.get(0).getInstructions()));
        assertTrue(recipeDTO.getContent().get(0).getLastUpdate().equals(recipeEntityList.get(0).getLastUpdate()));
        assertTrue(recipeDTO.getContent().get(0).getName().equals(recipeEntityList.get(0).getName()));
        assertTrue(recipeDTO.getContent().get(0).getServings().equals(recipeEntityList.get(0).getServings()));
        assertTrue(recipeDTO.getContent().get(0).getVegetarian().equals(recipeEntityList.get(0).getVegetarian()));
        assertTrue(recipeDTO.getContent().get(0).getUsername().equals(recipeEntityList.get(0).getUsername()));
        assertTrue(recipeDTO.getContent().size() == recipeEntityList.size());

    }

    /**
     * Test to ensure that converting a null RecipeEntity returns null.
     */
    @Test
    public void shouldReturnNullOnConvert() {

        final RecipeDTO pageRecipeDTO = new RecipeFromDomain().convert(null);
        assertTrue(pageRecipeDTO == null);

    }

    /**
     * Test to ensure that converting a null Page of RecipeEntities returns null.
     */
    @Test
    public void shouldReturnNullOnConvertPage() {

        final Page<RecipeDTO> pageRecipeDTO = RecipeFromDomain.convertEntityList(null);
        assertTrue(pageRecipeDTO == null);

    }

}
