package com.ooziesoft.recipeservice.controllers.dto;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.io.Serializable;
import org.junit.jupiter.api.Test;
/**
 * Tests for the RecipeDTO class.
 */
public class RecipeDTOTest implements Serializable {

    /**
     * Test to ensure that all fields of RecipeDTO are initialized to null.
     */
    @Test
    void shouldInitializeNullFields() {

        final RecipeDTO recipeDTO = new RecipeDTO();
        assertTrue(recipeDTO.getCreatedAt() == null);
        assertTrue(recipeDTO.getId() == null);
        assertTrue(recipeDTO.getIngredients() == null);
        assertTrue(recipeDTO.getInstructions() == null);
        assertTrue(recipeDTO.getLastUpdate() == null);
        assertTrue(recipeDTO.getName() == null);
        assertTrue(recipeDTO.getServings() == null);
        assertTrue(recipeDTO.getVegetarian() == null);
        assertTrue(recipeDTO.getUsername() == null);

    }

    /**
     * Test to ensure that a RecipeDTO can be correctly built from a builder with given values.
     */
    @Test
    void shouldBuildCorrectly() {

        final RecipeDTO recipeOriginal = RecipeDTO.createRandom();
        final RecipeDTO recipeDTO = RecipeDTO.builder().createdAt(recipeOriginal.getCreatedAt())
                .id(recipeOriginal.getId()).ingredients(recipeOriginal.getIngredients())
                .instructions(recipeOriginal.getInstructions()).lastUpdate(recipeOriginal.getLastUpdate())
                .name(recipeOriginal.getName()).servings(recipeOriginal.getServings())
                .username(recipeOriginal.getUsername()).vegetarian(recipeOriginal.getVegetarian()).build();
        assertTrue(recipeDTO.getCreatedAt().equals(recipeOriginal.getCreatedAt()));
        assertTrue(recipeDTO.getId().equals(recipeOriginal.getId()));
        assertTrue(recipeDTO.getIngredients().equals(recipeOriginal.getIngredients()));
        assertTrue(recipeDTO.getInstructions().equals(recipeOriginal.getInstructions()));
        assertTrue(recipeDTO.getLastUpdate().equals(recipeOriginal.getLastUpdate()));
        assertTrue(recipeDTO.getName().equals(recipeOriginal.getName()));
        assertTrue(recipeDTO.getServings().equals(recipeOriginal.getServings()));
        assertTrue(recipeDTO.getVegetarian().equals(recipeOriginal.getVegetarian()));
        assertTrue(recipeDTO.getUsername().equals(recipeOriginal.getUsername()));

    }

    /**
     * Test to verify that a RecipeDTO can be correctly built from another RecipeDTO instance and converted back to
     * builder.
     */
    @Test
    void shouldBuildFromRecipeDTOAndBackToBuilderCorrectly() {

        final RecipeDTO recipeOriginal = RecipeDTO.createRandom();
        final RecipeDTO recipeDTO = RecipeDTO.builder().createdAt(recipeOriginal.getCreatedAt())
                .id(recipeOriginal.getId()).ingredients(recipeOriginal.getIngredients())
                .instructions(recipeOriginal.getInstructions()).lastUpdate(recipeOriginal.getLastUpdate())
                .name(recipeOriginal.getName()).servings(recipeOriginal.getServings())
                .username(recipeOriginal.getUsername()).vegetarian(recipeOriginal.getVegetarian()).build();
        final RecipeDTO builtRecipe = recipeDTO.toBuilder().build();
        assertTrue(recipeDTO != builtRecipe);
        assertTrue(recipeDTO.getCreatedAt().equals(recipeOriginal.getCreatedAt()));
        assertTrue(recipeDTO.getId().equals(recipeOriginal.getId()));
        assertTrue(recipeDTO.getIngredients().equals(recipeOriginal.getIngredients()));
        assertTrue(recipeDTO.getInstructions().equals(recipeOriginal.getInstructions()));
        assertTrue(recipeDTO.getLastUpdate().equals(recipeOriginal.getLastUpdate()));
        assertTrue(recipeDTO.getName().equals(recipeOriginal.getName()));
        assertTrue(recipeDTO.getServings().equals(recipeOriginal.getServings()));
        assertTrue(recipeDTO.getUsername().equals(recipeOriginal.getUsername()));

    }

}
