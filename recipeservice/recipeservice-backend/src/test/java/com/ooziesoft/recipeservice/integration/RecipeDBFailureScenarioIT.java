package com.ooziesoft.recipeservice.integration;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ooziesoft.recipeservice.controllers.dto.RecipeDTO;
import com.ooziesoft.recipeservice.domain.model.RecipeEntity;
import com.ooziesoft.recipeservice.domain.repository.RecipeRepository;
import com.ooziesoft.recipeservice.utils.RandomUtils;
/**
 * Integration tests for simulating database access failures in the Recipe service. This class tests the system's
 * behavior under simulated conditions of database failures during various CRUD operations, ensuring that appropriate
 * HTTP status codes are returned.
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser
public class RecipeDBFailureScenarioIT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RecipeRepository recipeRepository;

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * Test to ensure the system returns HTTP 500 Internal Server Error on database access failure during GET request.
     */
    @Test
    public void shouldReturnInternalServerErrorWhenGetRequestFailsDueToDBAccess() throws Exception {

        when(recipeRepository.findById("1")).thenThrow(new RuntimeException());
        mockMvc.perform(get("/api/recipe/{id}", "1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError()).andReturn();

    }

    /**
     * Test to ensure the system returns HTTP 500 Internal Server Error on database access failure during POST request.
     */
    @Test
    public void shouldReturnInternalServerErrorWhenCreateRecipeFailsDueToDBAccess() throws Exception {

        final RecipeDTO dto = RecipeDTO.createRandom().toBuilder().id(null).createdAt(null).lastUpdate(null).build();
        when(recipeRepository.save(any(RecipeEntity.class))).thenThrow(new RuntimeException());
        mockMvc.perform(post("/api/recipe").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto))).andExpect(status().isInternalServerError()).andReturn();

    }

    /**
     * Test to ensure the system returns HTTP 500 Internal Server Error on database access failure during PUT request.
     */
    @Test
    public void shouldReturnInternalServerErrorWhenUpdateRecipeFailsDueToDBAccess() throws Exception {

        final String id = RandomUtils.randomId();
        final RecipeDTO dto = RecipeDTO.createRandom().toBuilder().id(null).createdAt(null).lastUpdate(null).build();
        when(recipeRepository.findById(id)).thenThrow(new RuntimeException());
        mockMvc.perform(put("/api/recipe/{id}", id).contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto))).andExpect(status().isInternalServerError()).andReturn();

    }

    /**
     * Test to ensure the system returns HTTP 500 Internal Server Error on database access failure during DELETE
     * request.
     */
    @Test
    public void shouldReturnInternalServerErrorWhenDeleteRecipeFailsDueToDBAccess() throws Exception {

        final String id = RandomUtils.randomId();
        when(recipeRepository.findById(id)).thenReturn(Optional.of(RecipeEntity.createRandom()));
        when(recipeRepository.existsById(id)).thenThrow(new RuntimeException());
        mockMvc.perform(delete("/api/recipe/{id}", id).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError()).andReturn();

    }

}
