CREATE TABLE IF NOT EXISTS recipe (servings integer not null, vegetarian boolean not null, created_at bigint not null, last_update bigint not null, id varchar(50) not null, 
username varchar(50) not null, name varchar(100) not null, ingredients varchar(1500) not null, instructions varchar(3000) not null, primary key (id));

CREATE TABLE IF NOT EXISTS users_entity (password varchar(255), role varchar(255), username varchar(255) not null, primary key (username));

DELIMITER //

CREATE PROCEDURE CreateIndexIfNotExists(
    IN tableName VARCHAR(64), 
    IN indexName VARCHAR(64), 
    IN columnName VARCHAR(64),
    IN indexType VARCHAR(10) -- 'FULLTEXT' or NULL/other for regular index
)
BEGIN
    DECLARE indexExists INT;

    SELECT COUNT(1) INTO indexExists
    FROM INFORMATION_SCHEMA.STATISTICS
    WHERE table_schema = DATABASE() AND
          table_name = tableName AND
          index_name = indexName;

    IF indexExists = 0 THEN
        IF indexType = 'FULLTEXT' THEN
            SET @s = CONCAT('CREATE FULLTEXT INDEX ', indexName, ' ON ', tableName, '(', columnName, ')');
        ELSE
            SET @s = CONCAT('CREATE INDEX ', indexName, ' ON ', tableName, '(', columnName, ')');
        END IF;
        
        PREPARE stmt FROM @s;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
    END IF;
END //

DELIMITER ;

CALL CreateIndexIfNotExists('recipe', 'ingredients_idx', 'ingredients', 'FULLTEXT');
CALL CreateIndexIfNotExists('recipe', 'instructions_idx', 'instructions', 'FULLTEXT');
CALL CreateIndexIfNotExists('recipe', 'vegetarian_idx', 'vegetarian', NULL);
CALL CreateIndexIfNotExists('recipe', 'servings_idx', 'servings', NULL);


