package com.ooziesoft.recipeservice.domain.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import com.ooziesoft.recipeservice.domain.model.RecipeEntity;
/**
 * RecipeRepository is a Spring Data JPA repository for RecipeEntity. It provides a range of standard database
 * operations for RecipeEntity objects, including CRUD operations and finder methods.
 */
public interface RecipeRepository extends JpaRepository<RecipeEntity, String> {
}
