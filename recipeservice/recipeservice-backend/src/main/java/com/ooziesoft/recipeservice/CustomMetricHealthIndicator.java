package com.ooziesoft.recipeservice;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class CustomMetricHealthIndicator implements HealthIndicator {

    @Override
    public Health health() {
        double freeMemoryPercentage = getFreeMemoryPercentage();

        if (freeMemoryPercentage > 20) { // Define your health logic
            return Health.up().withDetail("freeMemory", freeMemoryPercentage)
                    .withDetail("message", "Service is healthy!").build();
        } else {
            return Health.down().withDetail("freeMemory", freeMemoryPercentage)
                    .withDetail("message", "Custom metric is below the threshold!").build();
        }
    }

    private double getFreeMemoryPercentage() {
        // Get the Runtime instance
        Runtime runtime = Runtime.getRuntime();

        // Calculate memory usage
        long totalMemory = runtime.totalMemory(); // Total memory allocated to JVM
        long freeMemory = runtime.freeMemory(); // Free memory available in JVM
        long maxMemory = runtime.maxMemory(); // Maximum memory JVM can use

        long used = totalMemory - freeMemory;
        long maxFree = maxMemory - used;

        // Calculate free memory percentage
        double freeMemoryPercentage = ((double) maxFree / maxMemory) * 100;

        return freeMemoryPercentage;
    }
}