package com.ooziesoft.recipeservice;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
@AllArgsConstructor
@Schema(description = "Error response")
public class ErrorResponse {

    @JsonProperty
    private String claz;

    @JsonProperty
    private String description;

}
