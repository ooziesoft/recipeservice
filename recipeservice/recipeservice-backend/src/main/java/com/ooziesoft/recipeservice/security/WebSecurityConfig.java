package com.ooziesoft.recipeservice.security;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
/**
 * Configuration class for Spring Security.
 * <p>
 * It defines beans for UserDetailsService, PasswordEncoder, and SecurityFilterChain, configuring how user details are
 * loaded, how passwords are encoded, and the security filter chain that handles HTTP requests to the application.
 * </p>
 *
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig {

    /**
     * Configures the PasswordEncoder bean used in the application.
     *
     * <p>
     * This method sets up a BCryptPasswordEncoder for encoding and verifying passwords.
     * </p>
     *
     * @return PasswordEncoder implementation.
     */
    @Bean
    public PasswordEncoder passwordEncoder() {

        return new BCryptPasswordEncoder();

    }

    /**
     * Defines the security filter chain that handles HTTP requests.
     *
     * <p>
     * This method configures the HttpSecurity object, specifying how different types of requests should be secured. It
     * sets rules for request matchers, indicating which roles are required for different endpoints and also configures
     * HTTP Basic authentication.
     * </p>
     *
     * @param http The HttpSecurity object to be configured.
     * @return The configured SecurityFilterChain.
     * @throws Exception if an error occurs during configuration.
     */
    @Bean
    public SecurityFilterChain filterChain(final HttpSecurity http) throws Exception {

        http.headers().frameOptions().disable().and().csrf().disable().authorizeRequests().requestMatchers("/api/**")
                .hasRole("USER").requestMatchers("/version").permitAll().requestMatchers("/swagger-ui/**").permitAll()
                .and().httpBasic();
        return http.build();

    }

}
