package com.ooziesoft.recipeservice.service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.ooziesoft.recipeservice.controllers.Criteria;
import com.ooziesoft.recipeservice.controllers.dto.RecipeDTO;
/**
 * RecipeService is an interface defining operations for managing recipes. This includes creating, updating, retrieving,
 * and deleting recipes, as well as fetching all recipes with pagination and specific search criteria.
 */
public interface RecipeService {

    /**
     * Creates a new recipe.
     *
     * @param entity The RecipeDTO containing the recipe details to be created.
     * @return The created RecipeDTO.
     * @throws RecipeServiceException If an error occurs during the creation process.
     */
    RecipeDTO createRecipe(RecipeDTO entity) throws RecipeServiceException;

    /**
     * Updates an existing recipe.
     *
     * @param entity The RecipeDTO containing the updated recipe details.
     * @return The updated RecipeDTO.
     * @throws RecipeServiceException If an error occurs during the update process.
     */
    RecipeDTO updateRecipe(RecipeDTO entity) throws RecipeServiceException;

    /**
     * Retrieves a recipe by its ID.
     *
     * @param id The ID of the recipe to retrieve.
     * @return The requested RecipeDTO.
     * @throws RecipeServiceException If an error occurs during the retrieval process.
     */
    RecipeDTO getRecipe(String id) throws RecipeServiceException;

    /**
     * Deletes a recipe by its ID.
     *
     * @param id The ID of the recipe to be deleted.
     * @throws RecipeServiceException If an error occurs during the deletion process.
     */
    void deleteRecipe(String id) throws RecipeServiceException;

    /**
     * Retrieves all recipes based on given criteria, username, and pagination details.
     *
     * <p>
     * This method provides a way to retrieve a paginated list of recipes, filtered based on specified criteria and
     * associated with a particular username. It's useful for implementing features like user-specific recipe lists with
     * custom filtering and pagination.
     * </p>
     *
     * @param username The username associated with the recipes.
     * @param criteria The Criteria object containing filtering parameters.
     * @param pageable The Pageable object containing pagination parameters.
     * @return A Page of RecipeDTO objects that match the given criteria and belong to the specified user.
     * @throws RecipeServiceException If an error occurs during the retrieval process, such as database access issues or
     *                                invalid query parameters.
     */
    Page<RecipeDTO> getAllRecipes(String username, Criteria criteria, Pageable pageable) throws RecipeServiceException;

}
