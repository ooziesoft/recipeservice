package com.ooziesoft.recipeservice;
import java.util.Arrays;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MutablePropertySources;
/**
 * The Application class serves as the entry point for the Spring Boot application. It initializes and starts the Spring
 * Boot application.
 */
@SpringBootApplication
public class Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    /**
     * Main method which serves as the entry point for the Spring Boot application.
     *
     * @param args The command line arguments passed to the application.
     */
    public static void main(final String[] args) {

        LOGGER.info("SpringBootApplication starting...");
        // Creating and running the Spring application
        final SpringApplication app = new SpringApplication(Application.class);
        app.run(args);
        LOGGER.info("SpringBootApplication started successfully...");

    }

    /**
     * Event listener for the context refresh event. It logs the environment and configuration details.
     *
     * @param event The context refreshed event.
     */
    @EventListener
    public void handleContextRefresh(final ContextRefreshedEvent event) {

        final Environment env = event.getApplicationContext().getEnvironment();
        LOGGER.info("====== Environment and configuration ======");
        LOGGER.info("Active profiles: {}", Arrays.toString(env.getActiveProfiles()));
        // Logging environmental properties, excluding sensitive ones like credentials.
        final MutablePropertySources sources = ((AbstractEnvironment) env).getPropertySources();
        StreamSupport.stream(sources.spliterator(), false).filter(ps -> ps instanceof EnumerablePropertySource)
                .map(ps -> ((EnumerablePropertySource<?>) ps).getPropertyNames()).flatMap(Arrays::stream).distinct()
                .filter(prop -> !(prop.contains("credentials") || prop.contains("password")))
                .forEach(prop -> LOGGER.info("{}: {}", prop, env.getProperty(prop)));
        LOGGER.info("===========================================");

    }

}
