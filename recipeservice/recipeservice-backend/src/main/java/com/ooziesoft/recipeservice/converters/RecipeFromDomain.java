package com.ooziesoft.recipeservice.converters;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import com.ooziesoft.recipeservice.controllers.dto.RecipeDTO;
import com.ooziesoft.recipeservice.domain.model.RecipeEntity;
/**
 * RecipeFromDomain is a Spring component that implements the Converter interface to convert RecipeEntity objects to
 * RecipeDTO objects. This converter is used for transforming data from the domain model to the data transfer object
 * format suitable for API responses.
 */
@Component
public class RecipeFromDomain implements Converter<RecipeEntity, RecipeDTO> {

    /**
     * Converts a RecipeEntity object to a RecipeDTO object.
     *
     * @param source The RecipeEntity to convert.
     * @return The converted RecipeDTO object.
     */
    @Override
    public RecipeDTO convert(final RecipeEntity source) {

        return convertEntity(source);

    }

    /**
     * Static method to convert a RecipeEntity object to a RecipeDTO object.
     *
     * @param source The RecipeEntity to convert.
     * @return The converted RecipeDTO object or null if the source is null.
     */
    public static RecipeDTO convertEntity(final RecipeEntity source) {

        if (source == null) {

            return null;

        }

        return RecipeDTO.builder().id(source.getId()).username(source.getUsername()).createdAt(source.getCreatedAt())
                .lastUpdate(source.getLastUpdate())
                .ingredients(IngredientFromDomain.convertEntityList(source.getIngredients()))
                .instructions(source.getInstructions()).name(source.getName()).servings(source.getServings())
                .vegetarian(source.getVegetarian()).build();

    }

    /**
     * Converts a Page of RecipeEntity objects to a Page of RecipeDTO objects.
     *
     * @param source The Page of RecipeEntity objects to convert.
     * @return A Page of converted RecipeDTO objects or null if the source Page is null.
     */
    public static Page<RecipeDTO> convertEntityList(final Page<RecipeEntity> source) {

        if (source == null) {

            return null;

        }

        return source.map(RecipeFromDomain::convertEntity);

    }

}
