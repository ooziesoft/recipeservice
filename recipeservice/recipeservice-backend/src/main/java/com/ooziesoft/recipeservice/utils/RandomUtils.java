package com.ooziesoft.recipeservice.utils;
import java.util.UUID;
/**
 * RandomUtils is a utility class providing methods for generating random values. It includes functionalities to
 * generate random strings, integers, and universally unique identifiers (UUIDs).
 */
public class RandomUtils {

    /**
     * Generates a random string by appending a random integer to the provided string.
     *
     * @param str The base string to append a random integer to.
     * @return A new string composed of the original string and a random integer.
     */
    public static String randomString(final String str) {

        return str + "_" + randomInt();

    }

    /**
     * Generates a random integer.
     *
     * @return A randomly generated integer.
     */
    public static int randomInt() {

        return randomInt(Integer.MAX_VALUE - 1);

    }

    /**
     * Generates a random integer up to a maximum value.
     *
     * @param max The maximum value for the random integer.
     * @return A randomly generated integer up to the maximum value.
     */
    public static int randomInt(final int max) {

        return randomInt(0, max);

    }

    /**
     * Generates a random integer within a specified range.
     *
     * @param min The minimum value of the range.
     * @param max The maximum value of the range.
     * @return A randomly generated integer within the specified range.
     */
    public static int randomInt(final int min, final int max) {

        return min + (int) (Math.random() * (max + 1 - min));

    }

    /**
     * Generates a random UUID string.
     *
     * @return A randomly generated UUID string.
     */
    public static String randomId() {

        return UUID.randomUUID().toString();

    }

}
