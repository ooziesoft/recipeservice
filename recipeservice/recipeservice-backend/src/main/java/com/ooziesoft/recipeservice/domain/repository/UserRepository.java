package com.ooziesoft.recipeservice.domain.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import com.ooziesoft.recipeservice.domain.model.UsersEntity;
/**
 * Repository interface for User entities.
 *
 * <p>
 * This interface extends JpaRepository, leveraging Spring Data JPA to provide CRUD operations and additional query
 * methods for User entities. It is part of the domain layer of the application, interacting with the database to manage
 * User data.
 * </p>
 *
 * @see JpaRepository
 */
public interface UserRepository extends JpaRepository<UsersEntity, Long> {

    /**
     * Finds a user by their username.
     *
     * <p>
     * This method is a custom query method that allows for finding a user entity based on their username. It is useful
     * in scenarios such as authentication, where the username is used to retrieve the corresponding user details.
     * </p>
     *
     * @param username The username of the user to be found.
     * @return Users object if found, null otherwise.
     */
    UsersEntity findByUsername(String username);

}
