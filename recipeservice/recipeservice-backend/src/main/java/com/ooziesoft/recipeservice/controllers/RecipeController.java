package com.ooziesoft.recipeservice.controllers;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.ooziesoft.recipeservice.controllers.dto.RecipeDTO;
import com.ooziesoft.recipeservice.service.RecipeService;
import com.ooziesoft.recipeservice.service.RecipeServiceException;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import jakarta.validation.Valid;
/**
 * Controller class for handling recipe-related operations. This class interacts with the RecipeService to process data
 * and manage recipes.
 */
@RestController
@RequestMapping("/api/recipe")
@CrossOrigin(origins = "*")
public class RecipeController {

    private final RecipeService service;

    private final Logger logger = LoggerFactory.getLogger(RecipeController.class);

    /**
     * Autowires the RecipeService dependency to handle recipe operations. This constructor is used for dependency
     * injection.
     *
     * @param service The RecipeService instance that will be used for recipe operations.
     */
    @Autowired
    public RecipeController(final RecipeService service) {

        this.service = service;

    }

    /**
     * Retrieves a specific recipe by its ID. Fetches the recipe details for the provided recipe ID.
     *
     * @param id The ID of the recipe to retrieve.
     * @return ResponseEntity containing the requested RecipeDTO and HTTP status OK.
     * @throws RecipeServiceException If an error occurs during the retrieval process.
     */
    @Operation(summary = "Get a recipe by ID", description = "Retrieves detailed information "
            + "about a recipe with the given ID.", security = @SecurityRequirement(name = "basicAuth"))
    @ApiResponse(responseCode = "200", description = "Recipe found and returned successfully", content = @Content(mediaType = "application/json", schema = @Schema(implementation = RecipeDTO.class)))
    @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
    @ApiResponse(responseCode = "403", description = "Forbidden - You do not have permission to access this resource", content = @Content)
    @ApiResponse(responseCode = "404", description = "Recipe not found", content = @Content)
    @ApiResponse(responseCode = "500", description = "Internal Server Error - An unexpected error occurred while processing the request", content = @Content)
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<RecipeDTO> getRecipe(
            @PathVariable("id") @Parameter(description = "ID of the recipe to retrieve") final String id)
            throws RecipeServiceException {

        logger.debug("Getting recipe {}", id);
        final RecipeDTO dto = service.getRecipe(id);
        checkOwnership(dto.getUsername());
        return new ResponseEntity<>(dto, HttpStatus.OK);

    }

    /**
     * Handles the creation of a new recipe. Accepts recipe data in the form of a RecipeDTO and creates a new recipe
     * entry.
     *
     * @param dto The RecipeDTO containing the details of the recipe to be created.
     * @return ResponseEntity containing the created RecipeDTO and HTTP status CREATED.
     * @throws RecipeServiceException If an error occurs during the creation of the recipe.
     */
    @Operation(summary = "Create a new recipe", description = "Creates a new recipe with the provided data.", security = @SecurityRequirement(name = "basicAuth"))
    @ApiResponse(responseCode = "201", description = "Recipe created successfully", content = @Content(mediaType = "application/json", schema = @Schema(implementation = RecipeDTO.class)))
    @ApiResponse(responseCode = "400", description = "Invalid input", content = @Content)
    @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
    @ApiResponse(responseCode = "403", description = "Forbidden - You do not have permission to access this resource", content = @Content)
    @ApiResponse(responseCode = "500", description = "Internal Server Error - An unexpected error occurred while processing the request", content = @Content)
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<RecipeDTO> createRecipe(
            @Valid @RequestBody @Parameter(description = "Recipe data for creation") final RecipeDTO dto)
            throws RecipeServiceException {

        logger.debug("Creating recipe");
        final RecipeDTO recipeDTO = service.createRecipe(dto.toBuilder()
                .username(((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername())
                .build());
        return new ResponseEntity<>(recipeDTO, HttpStatus.CREATED);

    }

    /**
     * Handles the update of an existing recipe. Accepts recipe data along with the recipe ID and updates the
     * corresponding recipe.
     *
     * @param id  The ID of the recipe to update.
     * @param dto The RecipeDTO containing the updated details of the recipe.
     * @return ResponseEntity containing the updated RecipeDTO and the HTTP status.
     * @throws RecipeServiceException If an error occurs during the update process.
     */
    @Operation(summary = "Update an existing recipe", description = "Updates a recipe with the provided data for the given ID.", security = @SecurityRequirement(name = "basicAuth"))
    @ApiResponse(responseCode = "200", description = "Recipe updated successfully", content = @Content(mediaType = "application/json", schema = @Schema(implementation = RecipeDTO.class)))
    @ApiResponse(responseCode = "400", description = "Invalid input", content = @Content)
    @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
    @ApiResponse(responseCode = "403", description = "Forbidden - You do not have permission to access this resource", content = @Content)
    @ApiResponse(responseCode = "404", description = "Recipe not found", content = @Content)
    @ApiResponse(responseCode = "500", description = "Internal Server Error - An unexpected error occurred while processing the request", content = @Content)
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<RecipeDTO> updateRecipe(
            @PathVariable("id") @Parameter(description = "ID of the recipe to be updated") final String id,
            @Valid @RequestBody @Parameter(description = "Updated recipe data") final RecipeDTO dto)
            throws RecipeServiceException {

        checkOwnership(service.getRecipe(id).getUsername());
        final RecipeDTO updatedRecipe = service.updateRecipe(dto.toBuilder().id(id).build());
        return new ResponseEntity<>(updatedRecipe, HttpStatus.OK);

    }

    /**
     * Handles the deletion of an existing recipe. Deletes a recipe based on the provided recipe ID.
     *
     * @param id The ID of the recipe to delete.
     * @return ResponseEntity with HTTP status indicating the outcome of the operation.
     * @throws RecipeServiceException If an error occurs during the deletion process.
     */
    @Operation(summary = "Delete an existing recipe", description = "Deletes a recipe with the specified ID.", security = @SecurityRequirement(name = "basicAuth"))
    @ApiResponse(responseCode = "204", description = "Recipe deleted successfully", content = @Content)
    @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
    @ApiResponse(responseCode = "403", description = "Forbidden - You do not have permission to access this resource", content = @Content)
    @ApiResponse(responseCode = "404", description = "Recipe not found", content = @Content)
    @ApiResponse(responseCode = "500", description = "Internal Server Error - An unexpected error occurred while processing the request", content = @Content)
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteRecipe(
            @PathVariable("id") @Parameter(description = "ID of the recipe to be deleted") final String id)
            throws RecipeServiceException {

        logger.debug("Deleting recipe {}", id);
        checkOwnership(service.getRecipe(id).getUsername());
        service.deleteRecipe(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }

    /**
     * Retrieves a paginated list of all recipes based on the given criteria. This method supports pagination and
     * filtering based on the criteria provided.
     *
     * @param criteria The filtering criteria for querying recipes.
     * @param pageable The pagination information (page number, page size, sort order).
     * @return ResponseEntity containing a paginated list of recipes and HTTP status.
     * @throws RecipeServiceException If an error occurs during the query process.
     */
    @Operation(summary = "Get all recipes", description = "Retrieves a paginated list of recipes based on provided criteria.", security = @SecurityRequirement(name = "basicAuth"))
    @ApiResponse(responseCode = "200", description = "List of recipes retrieved successfully", content = @Content(mediaType = "application/json", schema = @Schema(implementation = PagedRecipeDTO.class)))
    @ApiResponse(responseCode = "400", description = "Invalid input", content = @Content)
    @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
    @ApiResponse(responseCode = "403", description = "Forbidden - You do not have permission to access this resource", content = @Content)
    @ApiResponse(responseCode = "500", description = "Internal Server Error - An unexpected error occurred while processing the request", content = @Content)
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Page<RecipeDTO>> getAllRecipes(@Valid @ParameterObject final Criteria criteria,
            @ParameterObject final Pageable pageable) throws RecipeServiceException {

        logger.debug("Querying recipes. Criteria: {}", criteria);
        final Page<RecipeDTO> recipes = service.getAllRecipes(
                ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername(), criteria,
                pageable);
        return new ResponseEntity<>(recipes, HttpStatus.OK);

    }

    /**
     * Checks if the current authenticated user is the owner of the resource.
     *
     * <p>
     * This method retrieves the current authenticated user's details and compares the username with the provided
     * ownerId. If the usernames do not match, it throws an AccessDeniedException, indicating that the current user does
     * not have permission to access the resource owned by ownerId.
     * </p>
     *
     * @param ownerId The ID of the owner to check against the current authenticated user.
     * @throws AccessDeniedException if the current authenticated user is not the owner.
     */
    private void checkOwnership(final String ownerId) throws AccessDeniedException {

        final User userDetails = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!userDetails.getUsername().equals(ownerId)) {

            throw new AccessDeniedException(userDetails.getUsername() + " is not authorized for the action.");

        }

    }

}
/**
 * A wrapper class for paginating {@link RecipeDTO} objects. This class extends {@link PageImpl} to provide additional
 * functionality for pagination in API responses. It is primarily used for Swagger documentation to accurately represent
 * paged responses in the API.
 *
 * @see RecipeDTO
 * @see PageImpl
 */
class PagedRecipeDTO extends PageImpl<RecipeDTO> {

    /**
     * Constructs a new PagedRecipeDTO.
     *
     * @param content  The list of {@link RecipeDTO} objects representing the content of the page.
     * @param pageable The pagination information.
     * @param total    The total number of items in the full list.
     */
    PagedRecipeDTO(final List<RecipeDTO> content, final Pageable pageable, final long total) {

        super(content, pageable, total);

    }

}
/**
 * Configuration class for setting up OpenAPI documentation.
 */
@Configuration
@SecurityScheme(name = "basicAuth", type = SecuritySchemeType.HTTP, scheme = "basic")
@OpenAPIDefinition(info = @Info(title = "Recipe API", version = "v1"))
class DocsConfiguration {
}
