package com.ooziesoft.recipeservice.controllers;
import java.util.Arrays;
import java.util.List;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Size;
/**
 * Criteria class represents the search and filter criteria for querying recipes. It encapsulates parameters such as
 * vegetarian status, servings, included and excluded ingredients, and a search term to refine recipe searches.
 */
public class Criteria {

    @Schema(description = "Filter for vegetarian recipes", example = "true")
    private Boolean vegetarian;

    @Schema(description = "Number of servings to filter recipes by", example = "4")
    private Integer servings;

    @ArraySchema(schema = @Schema(description = "List of ingredients that must be included in the recipes"))
    @Size(max = 5)
    private List<String> includedIngredients;

    @Size(max = 5)
    @ArraySchema(schema = @Schema(description = "List of ingredients that must not be included in the recipes"))
    private List<String> excludedIngredients;

    @Size(max = 20)
    @Schema(description = "String to be used for searching recipes")
    private String searchTerm;

    /**
     * Factory method to create a default Criteria instance with no specific filters.
     *
     * @return A new instance of Criteria.
     */
    public static Criteria any() {

        return new Criteria();

    }

    /**
     * Sets the vegetarian status for this criteria.
     *
     * @param vegetarian A Boolean indicating whether to filter for vegetarian recipes.
     */
    public void setVegetarian(final Boolean vegetarian) {

        this.vegetarian = vegetarian;

    }

    /**
     * Sets the number of servings for this criteria.
     *
     * @param servings The number of servings to filter recipes by.
     */
    public void setServings(final Integer servings) {

        this.servings = servings;

    }

    /**
     * Sets the list of ingredients to be included in the recipes.
     *
     * @param includedIngredients A list of ingredients that must be included in the recipes.
     */
    public void setIncludedIngredients(final List<String> includedIngredients) {

        this.includedIngredients = includedIngredients;

    }

    /**
     * Sets the list of ingredients to be excluded from the recipes.
     *
     * @param excludedIngredients A list of ingredients that must not be included in the recipes.
     */
    public void setExcludedIngredients(final List<String> excludedIngredients) {

        this.excludedIngredients = excludedIngredients;

    }

    /**
     * Sets the search term for this criteria.
     *
     * @param searchTerm A string to be used for searching recipes.
     */
    public void setSearchTerm(final String searchTerm) {

        this.searchTerm = searchTerm;

    }

    /**
     * Gets the vegetarian status filter of this criteria.
     *
     * @return A Boolean indicating whether the recipes should be vegetarian.
     */
    public Boolean getVegetarian() {

        return vegetarian;

    }

    /**
     * Gets the servings filter of this criteria.
     *
     * @return The number of servings to filter the recipes by.
     */
    public Integer getServings() {

        return servings;

    }

    /**
     * Gets the list of ingredients that should be included in the recipes.
     *
     * @return A list of ingredients to be included.
     */
    public List<String> getIncludedIngredients() {

        return includedIngredients;

    }

    /**
     * Gets the list of ingredients that should be excluded from the recipes.
     *
     * @return A list of ingredients to be excluded.
     */
    public List<String> getExcludedIngredients() {

        return excludedIngredients;

    }

    /**
     * Gets the search term of this criteria.
     *
     * @return A string representing the search term.
     */
    public String getSearchTerm() {

        return searchTerm;

    }

    @Override
    public String toString() {

        return "Criteria [vegetarian=" + vegetarian + ", servings=" + servings + ", includedIngredients="
                + Arrays.toString(includedIngredients != null ? includedIngredients.toArray() : null)
                + ", excludedIngredients="
                + Arrays.toString(excludedIngredients != null ? excludedIngredients.toArray() : null) + ", searchTerm="
                + searchTerm + "]";

    }

}
