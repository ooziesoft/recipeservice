package com.ooziesoft.recipeservice.service;
/**
 * RecipeServiceBadRequestException is a custom exception used within the recipe service to signify bad request
 * scenarios, specifically when an illegal argument is provided. This exception extends RecipeServiceException,
 * providing a more specific context for the error that occurred.
 */
public class RecipeServiceBadRequestException extends RecipeServiceException {

    /**
     * Constructs a new RecipeServiceBadRequestException based on an IllegalArgumentException.
     *
     * @param e The IllegalArgumentException that triggered this exception.
     */
    public RecipeServiceBadRequestException(final IllegalArgumentException e) {

        super(e);

    }

}
