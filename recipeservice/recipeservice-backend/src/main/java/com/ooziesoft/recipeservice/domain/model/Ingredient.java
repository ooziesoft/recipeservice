package com.ooziesoft.recipeservice.domain.model;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
/**
 * Ingredient represents an ingredient in the domain model. It contains information about the ingredient's name,
 * quantity, and unit.
 */
@Builder(toBuilder = true)
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Ingredient {

    private String name;

    private String quantity;

    private String unit;

}
