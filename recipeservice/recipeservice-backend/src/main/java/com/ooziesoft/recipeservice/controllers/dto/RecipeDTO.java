package com.ooziesoft.recipeservice.controllers.dto;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.ooziesoft.recipeservice.utils.RandomUtils;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
/**
 * RecipeDTO is a Data Transfer Object representing a recipe.
 */
@Schema(description = "Recipe Data Transfer Object")
@Builder(toBuilder = true)
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = { "id" })
public class RecipeDTO {

    @JsonProperty
    @NotEmpty
    @Size(max = 100)
    @Schema(description = "Name of the recipe", required = true)
    private String name;

    @JsonProperty
    @NotNull
    @Schema(description = "Indicates if the recipe is vegetarian", required = true)
    private Boolean vegetarian;

    @JsonProperty
    @NotNull
    @Positive
    @Schema(description = "Number of servings", required = true)
    private Integer servings;

    @JsonProperty
    @NotEmpty
    @Valid
    @Size(max = 10)
    @ArraySchema(schema = @Schema(implementation = IngredientDTO.class))
    @Schema(description = "List of ingredients", required = true)
    private List<IngredientDTO> ingredients;

    @JsonProperty
    @NotEmpty
    @Size(max = 3000)
    @Schema(description = "Cooking instructions", required = true)
    private String instructions;

    @Schema(description = "Owner of the recipe", accessMode = AccessMode.READ_ONLY)
    @JsonProperty(access = Access.READ_ONLY)
    private String username;

    /**
     * Unique identifier for this entity.
     */
    @Schema(description = "Unique identifier for this entity.", accessMode = AccessMode.READ_ONLY)
    @JsonProperty(access = Access.READ_ONLY)
    private String id;

    /**
     * Unix epoch time for the creation date.
     */
    @Schema(description = "Unix epoch time for last modification date.", accessMode = AccessMode.READ_ONLY)
    @JsonProperty(access = Access.READ_ONLY)
    private Long createdAt;

    /**
     * Unix epoch time for the last modification date.
     */
    @Schema(description = "Unix epoch time for last modification date.", accessMode = AccessMode.READ_ONLY)
    @JsonProperty(access = Access.READ_ONLY)
    private Long lastUpdate;

    /**
     * Creates a random RecipeDTO instance for testing or mock purposes.
     *
     * @return A randomly generated RecipeDTO instance.
     */
    public static RecipeDTO createRandom() {

        final int maxServings = 8;
        return RecipeDTO.builder().instructions(RandomUtils.randomString("instructions"))
                .name(RandomUtils.randomString("name")).servings(RandomUtils.randomInt(1, maxServings))
                .vegetarian(RandomUtils.randomInt(1) == 1 || false)
                .ingredients(List.of(new IngredientDTO("ingredient1", "1", "unit1"))).id(RandomUtils.randomId())
                .username("user").createdAt((long) RandomUtils.randomInt()).lastUpdate((long) RandomUtils.randomInt())
                .build();

    }

}
