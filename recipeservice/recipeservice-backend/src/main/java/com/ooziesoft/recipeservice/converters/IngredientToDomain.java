package com.ooziesoft.recipeservice.converters;
import java.util.List;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import com.ooziesoft.recipeservice.controllers.dto.IngredientDTO;
import com.ooziesoft.recipeservice.domain.model.Ingredient;
/**
 * IngredientToDomain is a Spring component that implements Converter to convert IngredientDTO objects to
 * Ingredient objects. It is used for transforming data from the data transfer object to the domain model,
 * typically for persistence operations.
 */
@Component
public class IngredientToDomain implements Converter<IngredientDTO, Ingredient> {

    /**
     * Converts an IngredientDTO object to an Ingredient object.
     *
     * @param source The IngredientDTO to convert.
     * @return The converted Ingredient object.
     */
    @Override
    public Ingredient convert(final IngredientDTO source) {

        return convertDTO(source);

    }

    /**
     * Static method to convert an IngredientDTO object to an Ingredient object.
     *
     * @param source The IngredientDTO to convert.
     * @return The converted Ingredient object or null if the source is null.
     */
    public static Ingredient convertDTO(final IngredientDTO source) {

        if (source == null) {

            return null;

        }

        return new Ingredient(source.getName(), source.getQuantity(), source.getUnit());

    }

    /**
     * Converts a list of IngredientDTO objects to a list of Ingredient objects.
     *
     * @param source The list of IngredientDTO objects to convert.
     * @return A list of converted Ingredient objects or null if the source list is null.
     */
    public static List<Ingredient> convertDTOList(final List<IngredientDTO> source) {

        if (source == null) {

            return null;

        }

        return source.stream().map(IngredientToDomain::convertDTO).toList();

    }

}
