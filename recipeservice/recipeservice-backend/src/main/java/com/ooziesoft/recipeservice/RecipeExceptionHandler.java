package com.ooziesoft.recipeservice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import com.ooziesoft.recipeservice.service.RecipeServiceBadRequestException;
import com.ooziesoft.recipeservice.service.RecipeServiceRecipeNotFoundException;
/**
 * Global exception handler for the RecipeService application. This class handles exceptions thrown by the application
 * and returns appropriate HTTP responses.
 */
@ControllerAdvice
public class RecipeExceptionHandler {

    private final Logger logger = LoggerFactory.getLogger(RecipeExceptionHandler.class);

    /**
     * Handles RecipeServiceBadRequestException and returns a BAD_REQUEST response.
     *
     * @param ex      The caught RecipeServiceBadRequestException.
     * @param request The web request during which the exception was raised.
     * @return ResponseEntity containing the exception message and HTTP status.
     */
    @ExceptionHandler({ RecipeServiceBadRequestException.class })
    public ResponseEntity<ErrorResponse> handleBadRequestException(final RecipeServiceBadRequestException ex,
            final WebRequest request) {

        logger.warn("Bad Request Exception: {}", ex.getMessage());
        return new ResponseEntity<ErrorResponse>(new ErrorResponse(ex.getClass().getName(), ex.toString()),
                HttpStatus.BAD_REQUEST);

    }

    /**
     * Handles RecipeServiceRecipeNotFoundException and returns a NOT_FOUND response.
     *
     * @param ex      The caught RecipeServiceRecipeNotFoundException.
     * @param request The web request during which the exception was raised.
     * @return ResponseEntity containing the exception message and HTTP status.
     */
    @ExceptionHandler({ RecipeServiceRecipeNotFoundException.class })
    public ResponseEntity<ErrorResponse> handleNotFoundException(final RecipeServiceRecipeNotFoundException ex,
            final WebRequest request) {

        logger.warn("Recipe Not Found: {}", ex.getEntityId());
        return new ResponseEntity<ErrorResponse>(
                new ErrorResponse(ex.getClass().getName(), "Recipe Not Found: " + ex.getEntityId()),
                HttpStatus.NOT_FOUND);

    }

    /**
     * Handles AccessDeniedException and returns an UNAUTHORIZED response.
     *
     * @param ex      The caught AccessDeniedException.
     * @param request The web request during which the exception was raised.
     * @return ResponseEntity containing the exception message and HTTP status.
     */
    @ExceptionHandler({ AccessDeniedException.class })
    public ResponseEntity<ErrorResponse> handleNotFoundException(final AccessDeniedException ex,
            final WebRequest request) {

        logger.warn(ex.toString());
        return new ResponseEntity<ErrorResponse>(new ErrorResponse(ex.getClass().getName(), ex.toString()),
                HttpStatus.UNAUTHORIZED);

    }

    /**
     * Handles generic RuntimeExceptions and returns an INTERNAL_SERVER_ERROR response.
     *
     * @param ex      The caught RuntimeException.
     * @param request The web request during which the exception was raised.
     * @return ResponseEntity containing the exception message and HTTP status.
     */
    @ExceptionHandler({ RuntimeException.class })
    public ResponseEntity<ErrorResponse> handleRuntimeException(final RuntimeException ex, final WebRequest request) {

        logger.error("Internal Server Error: {}", ex.getMessage());
        return new ResponseEntity<ErrorResponse>(new ErrorResponse(ex.getClass().getName(), ex.toString()),
                HttpStatus.INTERNAL_SERVER_ERROR);

    }

}
