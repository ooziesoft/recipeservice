package com.ooziesoft.recipeservice.converters;
import java.util.List;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import com.ooziesoft.recipeservice.controllers.dto.RecipeDTO;
import com.ooziesoft.recipeservice.domain.model.RecipeEntity;
/**
 * RecipeToDomain is a Spring component that implements the Converter interface to convert RecipeDTO objects to
 * RecipeEntity objects. This converter is used for transforming data from the data transfer object format back to the
 * domain model, typically for persistence operations.
 */
@Component
public class RecipeToDomain implements Converter<RecipeDTO, RecipeEntity> {

    /**
     * Converts a RecipeDTO object to a RecipeEntity object.
     *
     * @param source The RecipeDTO to convert.
     * @return The converted RecipeEntity object.
     */
    @Override
    public RecipeEntity convert(final RecipeDTO source) {

        return convertDTO(source);

    }

    /**
     * Static method to convert a RecipeDTO object to a RecipeEntity object.
     *
     * @param source The RecipeDTO to convert.
     * @return The converted RecipeEntity object or null if the source is null.
     */
    public static RecipeEntity convertDTO(final RecipeDTO source) {

        if (source == null) {

            return null;

        }

        return RecipeEntity.builder().id(source.getId()).username(source.getUsername()).createdAt(source.getCreatedAt())
                .lastUpdate(source.getLastUpdate())
                .ingredientEntities(IngredientToDomain.convertDTOList(source.getIngredients()))
                .instructions(source.getInstructions()).name(source.getName()).servings(source.getServings())
                .vegetarian(source.getVegetarian()).build();

    }

    /**
     * Converts a list of RecipeDTO objects to a list of RecipeEntity objects.
     *
     * @param source The list of RecipeDTO objects to convert.
     * @return A list of converted RecipeEntity objects or null if the source list is null.
     */
    public static List<RecipeEntity> convertDTOList(final List<RecipeDTO> source) {

        if (source == null) {

            return null;

        }

        return source.stream().map(entity -> convertDTO(entity)).toList();

    }

}
