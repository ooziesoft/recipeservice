package com.ooziesoft.recipeservice.security;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import com.ooziesoft.recipeservice.domain.model.UsersEntity;
import com.ooziesoft.recipeservice.domain.repository.UserRepository;
/**
 * Component for initializing test data in the application.
 *
 * <p>
 * This class is used to populate the database with test data when the application starts. It is active only in the
 * 'DEV' and 'UAT' profiles, ensuring that test data is not loaded in production environments.
 * </p>
 *
 * This component is active in 'DEV' and 'UAT' profiles.
 */
@Component
@Profile({ "LOCAL", "DEV", "UAT" })
public class TestData {

    private final Logger logger = LoggerFactory.getLogger(TestData.class);

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public TestData(final UserRepository userRepository, final PasswordEncoder passwordEncoder) {

        super();
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;

    }

    /**
     * Initializes test data when the application is ready.
     *
     * <p>
     * This method is an event listener that responds to the ApplicationReadyEvent. It creates and saves several user
     * entities in the database, including both regular users and administrators. This is useful for development and
     * user acceptance testing (UAT) environments where pre-populated data is necessary for testing.
     * </p>
     *
     * <p>
     * It catches and logs any exceptions that occur during the initialization of test data to avoid crashing the
     * application startup process.
     * </p>
     *
     * @param event The application ready event that triggers the initialization of test data.
     */
    @EventListener
    public void appReady(final ApplicationReadyEvent event) {

        try {

            logger.info("Saving demo data");
            userRepository.save(new UsersEntity("dev1", passwordEncoder.encode("dev1"), "ROLE_USER"));
            userRepository.save(new UsersEntity("dev2", passwordEncoder.encode("dev2"), "ROLE_USER"));
            userRepository.save(new UsersEntity("uat1", passwordEncoder.encode("uat1"), "ROLE_USER"));
            userRepository.save(new UsersEntity("uat2", passwordEncoder.encode("uat2"), "ROLE_USER"));
            userRepository.save(new UsersEntity("admin", passwordEncoder.encode("admin"), "ROLE_ADMIN"));
            logger.info("Demo data saved successfully.");

        } catch (final Exception e) {

            logger.warn("Can't save test data");

        }

    }

}
