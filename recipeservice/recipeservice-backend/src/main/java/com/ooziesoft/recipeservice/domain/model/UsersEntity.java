package com.ooziesoft.recipeservice.domain.model;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
/**
 * Entity class representing a user in the RecipeService application.
 *
 * <p>
 * It contains fields for the user's username, password, and role, with corresponding getters and setters. The username
 * field is the primary key of the entity.
 * </p>
 */
@Entity
@Builder(toBuilder = true)
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UsersEntity {

    @Id
    private String username;

    private String password;

    private String role;

}
