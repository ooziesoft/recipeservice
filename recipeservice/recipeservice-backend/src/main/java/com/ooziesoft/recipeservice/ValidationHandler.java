package com.ooziesoft.recipeservice;
import java.util.HashMap;
import java.util.Map;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
/**
 * ValidationHandler is a global exception handler that focuses on handling validation errors within the application. It
 * extends ResponseEntityExceptionHandler to leverage Spring's default exception handling mechanisms. The class
 * primarily deals with exceptions thrown during the validation of request bodies in controller methods, particularly
 * those annotated with @Valid. By intercepting MethodArgumentNotValidException, it customizes the response to provide
 * meaningful validation error information to the client.
 */
@ControllerAdvice
public class ValidationHandler extends ResponseEntityExceptionHandler {

    /**
     * Overrides the default method to handle validation errors encountered in controller methods. This method is
     * invoked when an argument annotated with @Valid fails validation.
     *
     * @param ex      The exception thrown when validation fails.
     * @param headers HTTP headers to be used in the response.
     * @param status  The HTTP status code to be used in the response.
     * @param request The current web request.
     * @return A ResponseEntity containing a map of validation errors and the BAD_REQUEST status.
     */
    @Override
    @Nullable
    protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex,
            final HttpHeaders headers, final HttpStatusCode status, final WebRequest request) {

        final Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {

            final String fieldName = ((FieldError) error).getField();
            final String message = error.getDefaultMessage();
            errors.put(fieldName, message);

        });
        return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);

    }

}
