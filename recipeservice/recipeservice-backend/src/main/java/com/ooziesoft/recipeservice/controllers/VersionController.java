package com.ooziesoft.recipeservice.controllers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
/**
 * Controller for health check and monitoring purposes
 */
@RestController
@RequestMapping(value = { "/version" })
@CrossOrigin(origins = "*")
public class VersionController {

    private final Logger logger = LoggerFactory.getLogger(VersionController.class);

    private final String version;

    /**
     * Constructor for {@link VersionController}. The application version is injected from the application properties.
     *
     * @param version The current version of the application, injected from {@code app.version} property.
     */
    @Autowired
    public VersionController(@Value("${app.version}") final String version) {

        this.version = version;

    }

    /**
     * Endpoint for retrieving the current version of the application. This method handles HTTP GET requests and returns
     * the application version.
     *
     * <p>
     * Response is encapsulated in {@link ResponseEntity} with a status of {@link HttpStatus#OK}.
     * </p>
     *
     * @return {@link ResponseEntity} containing the version string and the HTTP status.
     */
    @RequestMapping(method = { RequestMethod.GET })
    @Operation(summary = "Version info for healthcheck and management purposes")
    @ApiResponses({ @ApiResponse(responseCode = "200", content = {
            @Content(mediaType = "text/plain", schema = @Schema(implementation = String.class)) }, description = "Successful") })
    public ResponseEntity<String> getVersion() {

        logger.info("Version check performed: {}", version);
        return new ResponseEntity<>("Version:" + version, HttpStatus.OK);

    }

}
