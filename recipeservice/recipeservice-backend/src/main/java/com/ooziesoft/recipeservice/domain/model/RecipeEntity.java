package com.ooziesoft.recipeservice.domain.model;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ooziesoft.recipeservice.utils.RandomUtils;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
/**
 * RecipeEntity represents a recipe in the domain model. It contains details like name, vegetarian status, servings,
 * ingredients, and instructions.
 */
@Entity(name = "recipe")
@Builder(toBuilder = true)
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = { "id" })
public class RecipeEntity {

    private static Logger logger = LoggerFactory.getLogger(RecipeEntity.class);

    /**
     * The owner of the recipe.
     */
    @Column(nullable = false, length = 50)
    private String username;

    /**
     * The name of the recipe.
     */
    @Column(nullable = false, length = 100)
    private String name;

    /**
     * Indicates if the recipe is vegetarian.
     */
    @Column(nullable = false)
    private Boolean vegetarian;

    /**
     * Number of servings.
     */
    @Column(nullable = false)
    private Integer servings;

    /**
     * JSON representation of ingredients.
     */
    @Column(nullable = false, length = 1500)
    private String ingredients;

    /**
     * Cooking instructions.
     */
    @Column(nullable = false, length = 3000)
    private String instructions;

    /**
     * The unique identifier for this entity.
     */
    @Id
    @Column(nullable = false, length = 50)
    private String id;

    /**
     * The creation date in Unix epoch time.
     */
    @Column(nullable = false, precision = 13, scale = 0)
    private Long createdAt;

    /**
     * The last modification date in Unix epoch time.
     */
    @Column(nullable = false, precision = 13, scale = 0)
    private Long lastUpdate;

    /**
     * Deserializes the ingredients JSON string into a list of Ingredient objects.
     *
     * @return A list of Ingredient objects or null if deserialization fails.
     */
    public List<Ingredient> getIngredients() {

        if (ingredients == null) {

            return null;

        }

        try {

            return new ObjectMapper().readValue(ingredients, new TypeReference<List<Ingredient>>() {
            });

        } catch (final JsonProcessingException e) {

            logger.error(e.toString());

        }

        return null;

    }

    /**
     * Creates a random RecipeEntity instance for testing or mock purposes.
     *
     * @return A randomly generated RecipeEntity instance.
     */
    public static RecipeEntity createRandom() {

        final int maxServings = 8;
        return RecipeEntity.builder().instructions(RandomUtils.randomString("instructions"))
                .name(RandomUtils.randomString("name")).servings(RandomUtils.randomInt(1, maxServings))
                .vegetarian(RandomUtils.randomInt(1) == 1 || false)
                .ingredientEntities(List.of(new Ingredient("ingredient1", "1", "unit1")))
                .id(RandomUtils.randomId()).username("user").createdAt((long) RandomUtils.randomInt())
                .lastUpdate((long) RandomUtils.randomInt()).build();

    }

    // extend the generated builder class to add custom logic
    public static class RecipeEntityBuilder {

        /**
         * Converts the ingredients to a JSON string.
         *
         * @return The JSON representation of ingredients.
         */
        public RecipeEntityBuilder ingredientEntities(final List<Ingredient> ingredientEntities) {

            try {

                this.ingredients(new ObjectMapper().writeValueAsString(ingredientEntities));

            } catch (final JsonProcessingException e) {

                logger.error(e.toString());

            }

            return this;

        }

    }

}
