package com.ooziesoft.recipeservice.controllers.dto;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
/**
 * IngredientDTO is aclass representing an ingredient in a recipe.
 */
@Builder(toBuilder = true)
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class IngredientDTO {

    @JsonProperty
    @NotEmpty
    @Size(max = 50)
    @Schema(description = "The name of the ingredient.", required = true)
    private String name;

    @JsonProperty
    @NotEmpty
    @Size(max = 10)
    @Schema(description = "The quantity of the ingredient.", required = true)
    private String quantity;

    @JsonProperty
    @NotEmpty
    @Size(max = 10)
    @Schema(description = "The unit of measurement for the ingredient.", required = true)
    private String unit;

}
