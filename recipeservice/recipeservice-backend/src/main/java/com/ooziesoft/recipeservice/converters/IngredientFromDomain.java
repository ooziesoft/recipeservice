package com.ooziesoft.recipeservice.converters;
import java.util.List;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import com.ooziesoft.recipeservice.controllers.dto.IngredientDTO;
import com.ooziesoft.recipeservice.domain.model.Ingredient;
/**
 * IngredientFromDomain is a Spring component that implements Converter to convert Ingredient objects to
 * IngredientDTO objects. It is used for transforming data from the domain model to the data transfer object suitable
 * for API responses.
 */
@Component
public class IngredientFromDomain implements Converter<Ingredient, IngredientDTO> {

    /**
     * Converts an Ingredient object to an IngredientDTO object.
     *
     * @param source The Ingredient to convert.
     * @return The converted IngredientDTO object.
     */
    @Override
    public IngredientDTO convert(final Ingredient source) {

        return convertEntity(source);

    }

    /**
     * Static method to convert an Ingredient object to an IngredientDTO object.
     *
     * @param source The Ingredient to convert.
     * @return The converted IngredientDTO object or null if the source is null.
     */
    public static IngredientDTO convertEntity(final Ingredient source) {

        if (source == null) {

            return null;

        }

        return new IngredientDTO(source.getName(), source.getQuantity(), source.getUnit());

    }

    /**
     * Converts a list of Ingredient objects to a list of IngredientDTO objects.
     *
     * @param source The list of Ingredient objects to convert.
     * @return A list of converted IngredientDTO objects or null if the source list is null.
     */
    public static List<IngredientDTO> convertEntityList(final List<Ingredient> source) {

        if (source == null) {

            return null;

        }

        return source.stream().map(IngredientFromDomain::convertEntity).toList();

    }

}
