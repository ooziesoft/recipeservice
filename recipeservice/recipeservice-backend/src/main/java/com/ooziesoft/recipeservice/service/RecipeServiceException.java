package com.ooziesoft.recipeservice.service;
import java.util.Arrays;
import java.util.stream.Collectors;
/**
 * RecipeServiceException is a custom exception class for handling exceptions specific to the recipe service operations.
 * It extends the standard Exception class and provides various constructors for different error handling scenarios.
 */
public class RecipeServiceException extends Exception {

    /**
     * Constructs a new RecipeServiceException with concatenated error messages.
     *
     * @param messages An array of error messages to be concatenated.
     */
    public RecipeServiceException(final String... messages) {

        super(Arrays.stream(messages).collect(Collectors.joining(" ")));

    }

    /**
     * Constructs a new RecipeServiceException with a throwable cause.
     *
     * @param e The Throwable cause of this exception.
     */
    public RecipeServiceException(final Throwable e) {

        super("Exception occurred", e);

    }

}
