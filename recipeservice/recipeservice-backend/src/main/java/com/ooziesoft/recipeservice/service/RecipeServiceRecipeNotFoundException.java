package com.ooziesoft.recipeservice.service;
/**
 * RecipeServiceRecipeNotFoundException is a custom exception indicating that a specific recipe entity could not be
 * found. It extends RecipeServiceException and includes the ID of the missing entity for more detailed error reporting.
 */
public class RecipeServiceRecipeNotFoundException extends RecipeServiceException {

    /**
     * The unique identifier of the recipe that was not found.
     *
     * <p>
     * This field stores the ID of the recipe that the system attempted to retrieve but could not find. This identifier
     * is intended to assist in debugging and logging by providing specific information about which recipe entity was
     * missing.
     * </p>
     */
    private final String entityId;

    /**
     * Constructs a new RecipeServiceRecipeNotFoundException with the ID of the missing recipe entity.
     *
     * @param entityId The ID of the recipe entity that was not found.
     */
    public RecipeServiceRecipeNotFoundException(final String entityId) {

        super("Recipe not found: ", entityId);
        this.entityId = entityId;

    }

    /**
     * Retrieves the ID of the recipe entity that was not found.
     *
     * @return The ID of the missing recipe entity.
     */
    public String getEntityId() {

        return entityId;

    }

}
