package com.ooziesoft.recipeservice.security;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import com.ooziesoft.recipeservice.domain.model.UsersEntity;
import com.ooziesoft.recipeservice.domain.repository.UserRepository;
/**
 * Service to load user-specific data.
 *
 * <p>
 * This class implements the UserDetailsService interface provided by Spring Security. It is used to retrieve user
 * information from the database and convert it into a format that Spring Security can use for authentication and
 * authorization.
 * </p>
 *
 * @see UserDetailsService
 */
@Component
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    /**
     * Constructs a new UserDetailsServiceImpl with the specified UserRepository.
     *
     * @param userRepository The repository used for retrieving user data.
     */
    @Autowired
    public UserDetailsServiceImpl(final UserRepository userRepository) {

        this.userRepository = userRepository;

    }

    /**
     * Loads the user details by username.
     *
     * <p>
     * This method is used by Spring Security to load details about a user during authentication. It searches the user
     * repository using the provided username. If a user is found, it returns a User object containing the username,
     * password, and roles. If no user is found, it throws a UsernameNotFoundException.
     * </p>
     *
     * @param userName The username of the user whose details are to be loaded.
     * @return UserDetails representing the user's information.
     * @throws UsernameNotFoundException if the user is not found in the repository.
     */
    @Override
    public UserDetails loadUserByUsername(final String userName) throws UsernameNotFoundException {

        final UsersEntity user = userRepository.findByUsername(userName);

        if (user == null) {

            throw new UsernameNotFoundException("User is not Found");

        }

        return new User(user.getUsername(), user.getPassword(), List.of(new SimpleGrantedAuthority(user.getRole())));

    }

}
