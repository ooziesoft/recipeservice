package com.ooziesoft.recipeservice.service;
import static com.google.common.base.Preconditions.checkArgument;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.ooziesoft.recipeservice.controllers.Criteria;
import com.ooziesoft.recipeservice.controllers.dto.RecipeDTO;
import com.ooziesoft.recipeservice.converters.IngredientToDomain;
import com.ooziesoft.recipeservice.converters.RecipeFromDomain;
import com.ooziesoft.recipeservice.converters.RecipeToDomain;
import com.ooziesoft.recipeservice.domain.model.RecipeEntity;
import com.ooziesoft.recipeservice.domain.repository.RecipeRepository;
import com.ooziesoft.recipeservice.utils.RandomUtils;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
/**
 * RecipeServiceImpl is an implementation of the RecipeService interface. It provides methods for creating, updating,
 * retrieving, and deleting recipes, as well as searching for recipes based on criteria.
 */
@Service
public class RecipeServiceImpl implements RecipeService {

    private final Logger logger = LoggerFactory.getLogger(RecipeServiceImpl.class);

    private final RecipeRepository repository;

    private final EntityManager em;

    @Autowired
    public RecipeServiceImpl(final RecipeRepository repository, final EntityManager em) {

        super();
        this.repository = repository;
        this.em = em;

    }

    /**
     * Creates a new recipe based on the provided RecipeDTO.
     *
     * @param recipe The RecipeDTO representing the recipe to be created.
     * @return The created RecipeDTO.
     * @throws RecipeServiceException If there is an error during the creation process.
     */
    @Override
    public RecipeDTO createRecipe(final RecipeDTO recipe) throws RecipeServiceException {

        try {

            checkArgument(recipe.getId() == null, "Id should be null");
            checkArgument(recipe.getCreatedAt() == null, "Createdate should be null");
            checkArgument(recipe.getLastUpdate() == null, "Lastupdate should be null");
            final RecipeEntity updateEntity = RecipeToDomain.convertDTO(recipe).toBuilder().id(RandomUtils.randomId())
                    .createdAt(System.currentTimeMillis()).lastUpdate(System.currentTimeMillis()).build();
            repository.save(updateEntity);
            return RecipeFromDomain.convertEntity(updateEntity);

        } catch (final IllegalArgumentException e) {

            throw new RecipeServiceBadRequestException(e);

        }

    }

    /**
     * Updates an existing recipe based on the provided RecipeDTO.
     *
     * @param recipe The RecipeDTO representing the updated recipe.
     * @return The updated RecipeDTO.
     * @throws RecipeServiceException If there is an error during the update process.
     */
    @Override
    public RecipeDTO updateRecipe(final RecipeDTO recipe) throws RecipeServiceException {

        try {

            checkArgument(recipe.getId() != null, "Id shouldn't be null");
            checkArgument(recipe.getCreatedAt() == null, "Createdate should be null");
            checkArgument(recipe.getLastUpdate() == null, "Lastupdate should be null");
            RecipeEntity entity = repository.findById(recipe.getId()).get();
            entity = entity.toBuilder().ingredientEntities(IngredientToDomain.convertDTOList(recipe.getIngredients()))
                    .instructions(recipe.getInstructions()).name(recipe.getName()).servings(recipe.getServings())
                    .vegetarian(recipe.getVegetarian()).lastUpdate(System.currentTimeMillis()).build();
            repository.save(entity);
            return RecipeFromDomain.convertEntity(entity);

        } catch (final NoSuchElementException e) {

            throw new RecipeServiceRecipeNotFoundException(recipe.getId());

        } catch (final IllegalArgumentException e) {

            throw new RecipeServiceBadRequestException(e);

        }

    }

    /**
     * Retrieves a recipe with the specified ID.
     *
     * @param id The ID of the recipe to retrieve.
     * @return The RecipeDTO representing the retrieved recipe.
     * @throws RecipeServiceException If there is an error during the retrieval process.
     */
    @Override
    public RecipeDTO getRecipe(final String id) throws RecipeServiceException {

        try {

            checkArgument(id != null, "Id shouldn't be null");
            return RecipeFromDomain.convertEntity(repository.findById(id).get());

        } catch (final NoSuchElementException e) {

            throw new RecipeServiceRecipeNotFoundException(id);

        } catch (final IllegalArgumentException e) {

            throw new RecipeServiceBadRequestException(e);

        }

    }

    /**
     * Deletes a recipe with the specified ID.
     *
     * @param id The ID of the recipe to delete.
     * @throws RecipeServiceException If there is an error during the deletion process.
     */
    @Override
    public void deleteRecipe(final String id) throws RecipeServiceException {

        try {

            checkArgument(id != null, "Id shouldn't be null");

            if (repository.existsById(id)) {

                logger.warn("Recipe not found {}", id);
                repository.deleteById(id);

            } else {

                throw new RecipeServiceRecipeNotFoundException(id);

            }

        } catch (final IllegalArgumentException e) {

            throw new RecipeServiceBadRequestException(e);

        }

    }

    /**
     * Retrieves a paginated list of recipes based on the specified criteria and pageable settings.
     *
     * @param criteria The criteria for filtering recipes.
     * @param pageable The pageable settings for pagination.
     * @return A Page of RecipeDTO objects representing the retrieved recipes.
     * @throws RecipeServiceException If there is an error during the retrieval process.
     */
    @Override
    public Page<RecipeDTO> getAllRecipes(final String username, final Criteria criteria, final Pageable pageable)
            throws RecipeServiceException {

        final CriteriaBuilder cb = em.getCriteriaBuilder();
        final CriteriaQuery<RecipeEntity> cr = cb.createQuery(RecipeEntity.class);
        final Root<RecipeEntity> root = cr.from(RecipeEntity.class);
        cr.select(root);
        final List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.equal(root.get("username"), username));

        if (criteria.getVegetarian() != null) {

            if (criteria.getVegetarian()) {

                predicates.add(cb.isTrue(root.get("vegetarian")));

            } else {

                predicates.add(cb.isFalse(root.get("vegetarian")));

            }

        }

        if (criteria.getServings() != null) {

            predicates.add(cb.greaterThanOrEqualTo(root.get("servings"), criteria.getServings()));

        }

        if (criteria.getIncludedIngredients() != null) {

            for (final String ingredient : criteria.getIncludedIngredients()) {

                predicates.add(cb.like(root.get("ingredients"), "%" + ingredient + "%"));

            }

        }

        if (criteria.getExcludedIngredients() != null) {

            for (final String ingredient : criteria.getExcludedIngredients()) {

                predicates.add(cb.notLike(root.get("ingredients"), "%" + ingredient + "%"));

            }

        }

        if (criteria.getSearchTerm() != null) {

            predicates.add(cb.like(root.get("instructions"), "%" + criteria.getSearchTerm() + "%"));

        }

        final CriteriaQuery<RecipeEntity> cq = cr.select(root).where(predicates.toArray(new Predicate[0]));
        final Query query = em.createQuery(cq);
        final List<RecipeEntity> list = query.getResultList();
        final int start = (int) pageable.getOffset();
        final int end = Math.min(start + pageable.getPageSize(), list.size());
        return RecipeFromDomain.convertEntityList(new PageImpl<>(list.subList(start, end), pageable, list.size()));

    }

}
