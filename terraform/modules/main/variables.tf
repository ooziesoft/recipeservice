variable "rdsUser" {
  description = "rdsUser"
  type        = string
}

variable "rdsPassword" {
  description = "rdsPassword"
  type        = string
}

variable rdsPasswordSecretName {
  description = "rdsPassword Secret name"
  type = string
}

variable "service_name" {
  description = "service_name"
  type        = string
}

variable "cluster_name" {
  description = "cluster_name"
  type        = string
}

variable "docker_image" {
  description = "cluster_name"
  type        = string
}

variable container_port {
  description = "containerPort"
  type = number
}

variable health_check_path {
  description = "healthCheckPath"
  type = string
}

variable spring_profiles_active {
  description = "springProfilesActive"
  type = string
}

variable "dbName" {
  description = "dbName"
  type        = string
}