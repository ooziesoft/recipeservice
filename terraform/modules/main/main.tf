module "vpc" {
  source                 = "../vpc"
}

module "ecs" {
  source                 = "../ecs"
  public_subnet1         = module.vpc.public_subnet1
  public_subnet2         = module.vpc.public_subnet2
  private_subnet1        = module.vpc.private_subnet1
  private_subnet2        = module.vpc.private_subnet2
  security_group_public  = module.vpc.security_group_public
  security_group_private = module.vpc.security_group_private
  vpc_id                 = module.vpc.vpc_id
  docker_image           = var.docker_image       
  service_name           = var.service_name
  cluster_name           = var.cluster_name
  container_port         = var.container_port
  health_check_path      = var.health_check_path
  spring_profiles_active = var.spring_profiles_active
  rdsUser                = var.rdsUser
  rdsPassword            = var.rdsPassword
  rdsAddress             = module.rds.rds_address
  rdsPasswordSecretName  = var.rdsPasswordSecretName
}


module "rds" {
  source                 = "../rds"
  privateSubnet1            = module.vpc.private_subnet1
  privateSubnet2            = module.vpc.private_subnet2
  securityGroupRDS          = module.vpc.securityGroupRDS
  rdsUser                   = var.rdsUser
  rdsPassword               = var.rdsPassword
  dbName                    = var.dbName
}

