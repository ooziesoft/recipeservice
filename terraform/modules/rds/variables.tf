variable "privateSubnet1" {
  description = "privateSubnet1"
  type        = string
}

variable "privateSubnet2" {
  description = "privateSubnet2"
  type        = string
}

variable "securityGroupRDS" {
  description = "securityGroupRDS"
  type        = string
}

variable "rdsUser" {
  description = "rdsUser"
  type        = string
}

variable "rdsPassword" {
  description = "rdsPassword"
  type        = string
}

variable "dbName" {
  description = "dbName"
  type        = string
}