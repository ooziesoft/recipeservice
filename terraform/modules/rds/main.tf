resource "aws_db_subnet_group" "db_subnet_group" {
  name        = "my_db_subnet_group"
  description = "My DB Subnet Group"
  subnet_ids  = [var.privateSubnet1, var.privateSubnet2]
}

resource "aws_db_instance" "default" {
  allocated_storage    = 5
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "8.0"
  instance_class       = "db.t4g.medium"
  username             = var.rdsUser
  password             = var.rdsPassword
  parameter_group_name = "default.mysql8.0"
  db_subnet_group_name = aws_db_subnet_group.db_subnet_group.name
  vpc_security_group_ids = [var.securityGroupRDS]
  skip_final_snapshot  = true
  allow_major_version_upgrade = false
  auto_minor_version_upgrade = false
  db_name                   = var.dbName
}
