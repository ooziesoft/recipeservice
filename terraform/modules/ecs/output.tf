
output "dns_name" {
  description = "ELB DNS"
  value = aws_lb.load_balancer.dns_name
}

output "canonical_hosted_zone_id" {
  description = "ELB canonical hosted zone id"
  value = aws_lb.load_balancer.zone_id
}
