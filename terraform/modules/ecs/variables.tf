variable public_subnet1 {
  description = "publicSubnet1"
  type = string
}

variable public_subnet2 {
  description = "publicSubnet2"
  type = string
}

variable private_subnet1 {
  description = "privateSubnet1"
  type = string
}

variable private_subnet2 {
  description = "privateSubnet2"
  type = string
}

variable security_group_public {
  description = "securityGroupPublic"
  type = string
}

variable security_group_private {
  description = "securityGroupPrivate"
  type = string
}

variable vpc_id {
  description = "vpcId"
  type = string
}

variable docker_image {
  description = "dockerImage"
  type = string
}

variable service_name {
  description = "serviceName"
  type = string
}

variable cluster_name {
  description = "clusterName"
  type = string
}

variable container_port {
  description = "containerPort"
  type = number
}

variable health_check_path {
  description = "healthCheckPath"
  type = string
}

variable spring_profiles_active {
  description = "springProfilesActive"
  type = string
}


variable rdsUser {
  description = "rdsUser"
  type = string
}

variable rdsPassword {
  description = "rdsPassword"
  type = string
}

variable rdsPasswordSecretName {
  description = "rdsPassword Secret name"
  type = string
}


variable rdsAddress {
  description = "rdsAddress"
  type = string
}