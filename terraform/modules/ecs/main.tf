data "aws_region" "current" {}

resource "aws_ecs_cluster" "cluster" {
  name = var.cluster_name
}

resource "aws_ecs_task_definition" "task_definition" {
  family = join("", [var.service_name, "TaskDefinition"])
  network_mode = "awsvpc"
  requires_compatibilities = [
    "FARGATE"
  ]
  cpu = 256
  memory = "1024"
  execution_role_arn = aws_iam_role.execution_role.arn
  task_role_arn = aws_iam_role.task_role.arn
  container_definitions = jsonencode([
    {
      name = join("", [var.service_name, "Container"])
      image = var.docker_image
      portMappings = [
        {
          containerPort = var.container_port
        }
      ]
      "secrets": [
        {
          name = "spring.datasource.password",
          valueFrom = aws_secretsmanager_secret.rdsPassword.arn
        }
      ]
      environment = [
        {
          name = "spring.profiles.active",
          value = var.spring_profiles_active
        },
        {
          name = "DB_HOST",
          value = var.rdsAddress
        },
        {
          name = "spring.datasource.username",
          value = var.rdsUser
        }
      ]
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-region = data.aws_region.current.name,
          awslogs-group = aws_cloudwatch_log_group.log_group.id,
          awslogs-stream-prefix = "ecs"
        }
      }
    }
  ])
}

resource "aws_iam_role" "execution_role" {
  name = join("", [var.service_name, "ExecutionRole"])
  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow"
        Principal = {
          Service = "ecs-tasks.amazonaws.com"
        }
        Action = "sts:AssumeRole"
      }
    ]
  })
  managed_policy_arns = [
    "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
  ]
}

resource "aws_iam_role" "task_role" {
  name = join("", [var.service_name, "TaskRole"])
  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow"
        Principal = {
          Service = "ecs-tasks.amazonaws.com"
        }
        Action = "sts:AssumeRole"
      }
    ]
  })
}

resource "aws_iam_policy" "secretsmanager_policy" {
  name        = join("", [var.service_name, "SecretsManagerPolicy"])
  description = "Policy to allow access to Secrets Manager"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = [
          "secretsmanager:GetSecretValue",
          "secretsmanager:DescribeSecret"
        ],
        Resource = ["*"]
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "secretsmanager_attachment" {
  role       = aws_iam_role.execution_role.name
  policy_arn = aws_iam_policy.secretsmanager_policy.arn
  depends_on = [
    aws_iam_role.execution_role,
    aws_iam_policy.secretsmanager_policy
  ]
}

resource "aws_iam_role" "auto_scaling_role" {
  name = join("", [var.service_name, "AutoScalingRole"])
  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow"
        Principal = {
          Service = "ecs-tasks.amazonaws.com"
        }
        Action = "sts:AssumeRole"
      }
    ]
  })
  managed_policy_arns = [
    "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceAutoscaleRole"
  ]
}

resource "aws_ecs_service" "service" {
  name = var.service_name
  cluster = aws_ecs_cluster.cluster.arn
  task_definition = aws_ecs_task_definition.task_definition.arn
  deployment_minimum_healthy_percent = 50
  deployment_maximum_percent = 200
  desired_count = 1
  health_check_grace_period_seconds = 180
  launch_type = "FARGATE"
  network_configuration {
    assign_public_ip = false
    security_groups = [var.security_group_private]
    subnets         = [var.private_subnet1,var.private_subnet2]
  }
  load_balancer {
      container_name = join("", [var.service_name, "Container"])
      container_port = var.container_port
      target_group_arn = aws_lb_target_group.target_group.id
  }
}

resource "aws_lb_target_group" "target_group" {
  health_check {
    interval            = 120
    timeout             = 10
    path                = var.health_check_path
    healthy_threshold   = 2
    unhealthy_threshold = 3
    matcher             = "200-299"
  }
  name = join("", [var.service_name, "TargetGroup"])
  port = var.container_port
  protocol = "HTTP"
  vpc_id = var.vpc_id
  target_type = "ip"
  deregistration_delay =20
}

resource "aws_lb_listener" "listener_http" {
  load_balancer_arn = aws_lb.load_balancer.id
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.target_group.arn
  }
}

resource "aws_lb" "load_balancer" {
  idle_timeout =60
  name = join("", [var.service_name, "LB"])
  security_groups = [var.security_group_public]
  subnets = [var.public_subnet1,var.public_subnet2]
}


resource "aws_cloudfront_distribution" "elb_cloudfront_distribution" {
  enabled             = true
  is_ipv6_enabled     = true
  default_root_object = "index.html"

  origin {
    domain_name = aws_lb.load_balancer.dns_name
    origin_id   = aws_lb.load_balancer.id

    custom_origin_config {
      http_port                = 80
      https_port               = 443
      origin_protocol_policy   = "match-viewer"
      origin_ssl_protocols     = ["TLSv1.2"]
    }
  }

  default_cache_behavior {
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = aws_lb.load_balancer.id

    forwarded_values {
      query_string = true

      cookies {
        forward = "all"
      }
      
      headers = ["*"]  // Forward all headers
      
    }

    viewer_protocol_policy = "allow-all"
    min_ttl                = 0
    default_ttl            = 0
    max_ttl                = 0
  }

  // ... Additional configurations like PriceClass, Aliases, SSL Certificate, etc. ...

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    cloudfront_default_certificate = true
  }
}


resource "aws_cloudwatch_log_group" "log_group" {
  name = join("", ["/ecs/", var.service_name, "TaskDef"])
}

resource "aws_appautoscaling_target" "auto_scaling_target" {
  min_capacity = 1
  max_capacity = 2
  resource_id = join("/", ["service", aws_ecs_cluster.cluster.arn, aws_ecs_service.service.name])
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace = "ecs"
  role_arn = aws_iam_role.auto_scaling_role.arn
}

resource "aws_appautoscaling_policy" "auto_scaling_policy" {
  name               = "scale-down"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.auto_scaling_target.resource_id
  scalable_dimension = aws_appautoscaling_target.auto_scaling_target.scalable_dimension
  service_namespace  = "ecs"
  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }

    target_value       = 50
    scale_in_cooldown  = 10
    scale_out_cooldown = 10
  }

}

resource "aws_secretsmanager_secret" "rdsPassword" {
  name                           = var.rdsPasswordSecretName
}

resource "aws_secretsmanager_secret_version" "rdsPassword" {
  secret_id = aws_secretsmanager_secret.rdsPassword.id
  secret_string = var.rdsPassword
}
