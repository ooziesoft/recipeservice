data "aws_region" "current" {}

resource "aws_vpc" "vpc" {
  cidr_block = "10.0.0.0/16"
  instance_tenancy = "default"
  enable_dns_support = "true"
  enable_dns_hostnames = "false"
}

resource "aws_security_group" "security_group_public" {
  name = "securityGroupPublic"
  description = "Public SG"
  vpc_id = aws_vpc.vpc.id
}

resource "aws_security_group_rule" "security_group_public_ingress_80" {
    type = "ingress"
    from_port = 80
    to_port = 80
    protocol = "tcp"
    security_group_id = aws_security_group.security_group_public.id
    cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "security_group_public_ingress_443" {
    type = "ingress"
    from_port = 443
    to_port = 443
    protocol = "tcp"
    security_group_id = aws_security_group.security_group_public.id
    cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "security_group_public_egress_8080" {
    type = "egress"
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    security_group_id = aws_security_group.security_group_public.id
    source_security_group_id = aws_security_group.security_group_private.id
}

resource "aws_security_group" "security_group_private" {
  name = "securityGroupPrivate"
  description = "Private SG"
  vpc_id = aws_vpc.vpc.id
}

resource "aws_security_group_rule" "security_group_private_egress_443" {
    type = "egress"
    from_port = 443
    to_port = 443
    protocol = "tcp"
    security_group_id = aws_security_group.security_group_private.id
    cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "security_group_private_ingress_8080" {
    type = "ingress"
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    security_group_id = aws_security_group.security_group_private.id
    source_security_group_id = aws_security_group.security_group_public.id
}

resource "aws_security_group_rule" "security_group_private_egress_3306" {
    type = "egress"
    from_port = 3306
    to_port = 3306
    protocol = "tcp"
    security_group_id = aws_security_group.security_group_private.id
    source_security_group_id = aws_security_group.securityGroupRDS.id
}

resource "aws_security_group" "securityGroupRDS" {
  name = "securityGroupRDS"
  description = "securityGroupRDS"
  vpc_id = aws_vpc.vpc.id
  ingress {
    protocol = "tcp"
    from_port = 3306
    to_port = 3306
    security_groups = [aws_security_group.security_group_private.id]
  }
}


resource "aws_internet_gateway" "internet_gateway" {}

resource "aws_internet_gateway_attachment" "vpc_gateway_attachment" {
  vpc_id = aws_vpc.vpc.id
  internet_gateway_id = aws_internet_gateway.internet_gateway.id
}

resource "aws_subnet" "public_subnet1" {
  availability_zone = join("", [data.aws_region.current.name, "a"])
  map_public_ip_on_launch = "true"
  cidr_block = "10.0.1.0/24"
  vpc_id = aws_vpc.vpc.id
  tags = {
    name = "publicSubnet1"
  }
}

resource "aws_subnet" "public_subnet2" {
  availability_zone = join("", [data.aws_region.current.name, "b"])
  map_public_ip_on_launch = "true"
  cidr_block = "10.0.2.0/24"
  vpc_id = aws_vpc.vpc.id
  tags = {
    name = "publicSubnet2"
  }
}

resource "aws_subnet" "private_subnet1" {
  availability_zone = join("", [data.aws_region.current.name, "a"])
  cidr_block = "10.0.10.0/24"
  vpc_id = aws_vpc.vpc.id
  tags = {
    name = "privateSubnet1"
  }
}

resource "aws_subnet" "private_subnet2" {
  availability_zone = join("", [data.aws_region.current.name, "b"])
  cidr_block = "10.0.11.0/24"
  vpc_id = aws_vpc.vpc.id
  tags = {
    name = "privateSubnet2"
  }
}

resource "aws_network_acl" "network_acl_public" {
  vpc_id = aws_vpc.vpc.id
}

resource "aws_network_acl" "network_acl_private" {
  vpc_id = aws_vpc.vpc.id
}

resource "aws_network_acl_rule" "network_acl_entry_public1" {
  egress = true
  protocol   = "6"
  rule_number    = 100
  rule_action = "allow"
  cidr_block = "0.0.0.0/0"
  from_port  = 80
  to_port    = 80
  network_acl_id = aws_network_acl.network_acl_public.id
}

resource "aws_network_acl_rule" "network_acl_entry_public2" {
  egress = false
  protocol   = "6"
  rule_number    = 100
  rule_action = "allow"
  cidr_block = "0.0.0.0/0"
  from_port  = 80
  to_port    = 80
  network_acl_id = aws_network_acl.network_acl_public.id
}

resource "aws_network_acl_rule" "network_acl_entry_public3" {
  egress = true
  protocol   = "6"
  rule_number    = 200
  rule_action = "allow"
  cidr_block = "0.0.0.0/0"
  from_port  = 443
  to_port    = 443
  network_acl_id = aws_network_acl.network_acl_public.id
}

resource "aws_network_acl_rule" "network_acl_entry_public4" {
  egress = false
  protocol   = "6"
  rule_number    = 200
  rule_action = "allow"
  cidr_block = "0.0.0.0/0"
  from_port  = 443
  to_port    = 443
  network_acl_id = aws_network_acl.network_acl_public.id
}

resource "aws_network_acl_rule" "network_acl_entry_public5" {
  egress = true
  protocol   = "6"
  rule_number    = 300
  rule_action = "allow"
  cidr_block = "0.0.0.0/0"
  from_port  = 22
  to_port    = 22
  network_acl_id = aws_network_acl.network_acl_public.id
}

resource "aws_network_acl_rule" "network_acl_entry_public6" {
  egress = false
  protocol   = "6"
  rule_number    = 300
  rule_action = "allow"
  cidr_block = "0.0.0.0/0"
  from_port  = 22
  to_port    = 22
  network_acl_id = aws_network_acl.network_acl_public.id
}

resource "aws_network_acl_rule" "network_acl_entry_public7" {
  egress = true
  protocol   = "6"
  rule_number    = 400
  rule_action = "allow"
  cidr_block = "0.0.0.0/0"
  from_port  = 1024
  to_port    = 65535
  network_acl_id = aws_network_acl.network_acl_public.id
}

resource "aws_network_acl_rule" "network_acl_entry_public8" {
  egress = false
  protocol   = "6"
  rule_number    = 400
  rule_action = "allow"
  cidr_block = "0.0.0.0/0"
  from_port  = 1024
  to_port    = 65535
  network_acl_id = aws_network_acl.network_acl_public.id
}

resource "aws_network_acl_rule" "network_acl_entry_private1" {
  egress = true
  protocol   = "-1"
  rule_number    = 100
  rule_action     = "allow"
  cidr_block = "0.0.0.0/0"
  from_port  = 0
  to_port    = 0
  network_acl_id = aws_network_acl.network_acl_private.id
}

resource "aws_network_acl_rule" "network_acl_entry_private2" {
  egress = false
  protocol   = "-1"
  rule_number    = 100
  rule_action     = "allow"
  cidr_block = "0.0.0.0/0"
  from_port  = 0
  to_port    = 0
  network_acl_id = aws_network_acl.network_acl_private.id
}

resource "aws_network_acl_association" "subnet_network_acl_association_public1" {
  network_acl_id = aws_network_acl.network_acl_public.id
  subnet_id = aws_subnet.public_subnet1.id
}

resource "aws_network_acl_association" "subnet_network_acl_association_public2" {
  network_acl_id = aws_network_acl.network_acl_public.id
  subnet_id = aws_subnet.public_subnet2.id
}

resource "aws_network_acl_association" "subnet_network_acl_association_private1" {
  network_acl_id = aws_network_acl.network_acl_private.id
  subnet_id = aws_subnet.private_subnet1.id
}

resource "aws_network_acl_association" "subnet_network_acl_association_private2" {
  network_acl_id = aws_network_acl.network_acl_private.id
  subnet_id = aws_subnet.private_subnet2.id
}

resource "aws_route_table" "route_table_private" {
  vpc_id = aws_vpc.vpc.id
}

resource "aws_route_table" "route_table_public" {
  vpc_id = aws_vpc.vpc.id
}

resource "aws_route_table_association" "subnet_route_table_association_private1" {
  route_table_id = aws_route_table.route_table_private.id
  subnet_id = aws_subnet.private_subnet1.id
}

resource "aws_route_table_association" "subnet_route_table_association_private2" {
  route_table_id = aws_route_table.route_table_private.id
  subnet_id = aws_subnet.private_subnet2.id
}

resource "aws_route_table_association" "subnet_route_table_association_public1" {
  route_table_id = aws_route_table.route_table_public.id
  subnet_id = aws_subnet.public_subnet1.id
}

resource "aws_route_table_association" "subnet_route_table_association_public2" {
  route_table_id = aws_route_table.route_table_public.id
  subnet_id = aws_subnet.public_subnet2.id
}

resource "aws_route" "route_to_internet_gateway1" {
  destination_cidr_block = "0.0.0.0/0"
  route_table_id = aws_route_table.route_table_public.id
  gateway_id = aws_internet_gateway.internet_gateway.id
}

resource "aws_eip" "eip_nat_gateway" {
}

resource "aws_nat_gateway" "nat_gateway" {
  allocation_id = aws_eip.eip_nat_gateway.id
  subnet_id = aws_subnet.public_subnet1.id
}

resource "aws_route" "route_to_nat_gateway" {
  route_table_id = aws_route_table.route_table_private.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id = aws_nat_gateway.nat_gateway.id
}
