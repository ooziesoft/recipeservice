
output "vpc_id" {
  value = aws_vpc.vpc.id
}

output "public_subnet1" {
  value = aws_subnet.public_subnet1.id
}

output "public_subnet2" {
  value = aws_subnet.public_subnet2.id
}

output "private_subnet1" {
  value = aws_subnet.private_subnet1.id
}

output "private_subnet2" {
  value = aws_subnet.private_subnet2.id
}

output "security_group_public" {
  value = aws_security_group.security_group_public.id
}

output "security_group_private" {
  value = aws_security_group.security_group_private.id
}
output "securityGroupRDS" {
  value = aws_security_group.securityGroupRDS.id
}