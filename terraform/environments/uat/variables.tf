variable "account_id" {
  type    = string
  default = "658576402789"
}

variable "rdsUser" {
  description = "rdsUser"
  type        = string
}

variable "rdsPassword" {
  description = "rdsPassword"
  type        = string
}

variable rdsPasswordSecretName {
  description = "rdsPassword Secret name"
  type = string
}

variable "dockerImage" {
  description = "dockerImage"
  type        = string
}

variable container_port {
  description = "containerPort"
  type = number
  default = 8080
}

variable health_check_path {
  description = "healthCheckPath"
  type = string
  default = "/swagger-ui/index.html"
}

variable "dbName" {
  description = "dbName"
  type        = string
}

variable "moduleName" {
  description = "moduleName"
  type        = string
}