terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.18.0"
    }
  }

  backend "s3" {
    bucket         	 = "ooziesoft-recipeservice-terraform"
    key              = "state/uat/terraform.tfstate"
    region         	 = "eu-central-1"
  }
}

module "main" {
  source                 = "../../modules/main"
  rdsUser                = var.rdsUser
  rdsPassword            = var.rdsPassword
  rdsPasswordSecretName  = var.rdsPasswordSecretName
  service_name           = "${var.moduleName}srv"
  cluster_name           = "${var.moduleName}cls"
  docker_image           = var.dockerImage
  health_check_path      = var.health_check_path
  spring_profiles_active = "UAT"
  container_port         = var.container_port
  dbName                 = var.dbName
}
