# RecipeService Project
## Introduction
Welcome to **RecipeService**, a RESTful API designed to  manage your recipes.


## Key Components:
- **[API Service (Spring Boot)](recipeservice/recipeservice-backend)**: Managing all REST operations.
- **Database (MySQL & H2)**: H2 to run locally on IDE and on CI; MYSQL/RDS on [development](docker-compose.yml#L3) and [UAT/production](terraform/modules/rds/main.tf#L7) environments.
- **[Nginx](nginx)**: To replicate the load balancer behavior of UAT/production environments in a development setting.
- **Docker/Docker-compose**: Core of distributing and running the application
- **[Container Service(AWS ECS)](terraform/modules/ecs/main.tf#L133)**: UAT/production deployment.

## Project Architecture
**RecipeService** follows a microservice architecture paradigm, designed for high scalability and manageability. 

<img src="files/architecture.png"  width="90%" height="90%">

The UAT/production environment is centered around a ECS cluster, integrated with a CI/CD pipeline. Leveraging ECS allows for efficient management of the system and allows to easily scale the application. The setup of a ECS deployment includes several components:

- **[ECS](terraform/modules/ecs/main.tf#L133)**: AWS Container orchestration service.
- **[Cloud Front](terraform/modules/ecs/main.tf#L190)**: CloudFront is AWS's content delivery network (CDN) service that uses a global network of edge locations to deliver content with high availability and performance. This Terraform configuration is responsible for setting up CloudFront as part of the ECS deployment, optimizing content delivery for the application.
- **[ELB](terraform/modules/ecs/main.tf#L182)**: Distributes incoming application traffic across multiple targets.
- **ECR**: his is a fully-managed Docker container registry service offered by AWS. It's established outside of Terraform's management in order to decouple the codebase from the deployment process. This separation helps in breaking dependencies between the application's code and its deployment mechanisms.
- **[CloudWatch](terraform/modules/ecs/main.tf#L243)**: Used for monitoring logs.
- **[AutoScaling Policy](terraform/modules/ecs/main.tf#L247)**: Automatically adjusts the number of instances in response to the observed CPU usage.
- **[RDS](terraform/modules/rds/main.tf#L7)**: Managed relational database service with MySQL version 8.0.
- **[Secret Manager](terraform/modules/ecs/main.tf#L274)**: This is a reference to the AWS Secrets Manager configuration. AWS Secrets Manager is a service for securely storing and managing sensitive information, such as database passwords.
- **[SSM Parameter](terraform-ci.yml#L90)**: This refers to the AWS Systems Manager (SSM) Parameter Store configuration. AWS SSM Parameter Store provides a centralized service to manage configuration data and secrets. In this context, it's utilized for securely storing the Elastic Load Balancer (ELB) DNS name. This stored DNS name is then accessed and used within the CI/CD pipeline, specifically for Postman testing, ensuring that Postman tests can dynamically retrieve and use the correct ELB endpoint.
- **[Flyway](recipeservice/recipeservice-backend/src/main/resources/application-LIVE.yml#L6)**: Flyway is integrated into the Spring Boot application lifecycle for database schema migration. It's utilized to create and manage the database schema, ensuring that schema changes are systematically applied as part of the application's deployment and update processes. This integration facilitates smooth and consistent updates to the database structure in line with the application's evolution.

##### UAT Deployment:

Test Users(Username/Password): *dev1/dev1, dev2/dev2, uat1/uat1, uat2/uat2*


[Resource URL](http://recipesrvLB-194333437.eu-central-1.elb.amazonaws.com/api/recipe):

```
http://recipesrvLB-194333437.eu-central-1.elb.amazonaws.com/api/recipe
```

[Swagger UI](http://recipesrvLB-194333437.eu-central-1.elb.amazonaws.com/swagger-ui/index.html):

```
http://recipesrvLB-194333437.eu-central-1.elb.amazonaws.com/swagger-ui/index.html
```

[Resource URL(Cloud Front)](http://d1gqemd4azf6bo.cloudfront.net/api/recipe):

```
http://d1gqemd4azf6bo.cloudfront.net/api/recipe
```

[Swagger UI(Cloud Front)](http://d1gqemd4azf6bo.cloudfront.net/swagger-ui/index.html):

```
http://d1gqemd4azf6bo.cloudfront.net/swagger-ui/index.html
```

##### CI/CD Pipeline:

The [CI/CD](.gitlab-ci.yml) pipeline is the backbone of the deployment strategy. Here's how it works:

- **[Automated Testing](.gitlab-ci.yml?ref_type=heads#L31)**: Upon merging code into the master branch, it goes through a series of testing phases, including controller, unit, and isolated integration testing. To facilitate these tests, I employ a containerized environment that utilizes an H2 in-memory database, which is particularly suitable for integration testing purposes. In specific integration tests, I simulate failure scenarios by mocking the database layer to assess the system's resilience.
- **[Docker Image Creation](.gitlab-ci.yml?ref_type=heads#L40)**: Post-testing, production-ready Docker images are created and pushed to ECR.
- **[Deployment to UAT environment](.gitlab-ci.yml?ref_type=heads#L56)**: This step involves deploying to a UAT environment.
- **[E2E Tests](.gitlab-ci.yml?ref_type=heads#L66)**: End-to-end tests run on this stage.
- **Deployment to Production**: The final stage involves deploying to the production environment. At the current phase of the project, we don't deploy to production, just UAT.
- **[Terraform](.gitlab-ci.yml?ref_type=heads#L17)**: Terraform is utilized for IaC on AWS.

<img src="files/cicd.png"  width="100%" height="100%">

## Prerequisites
- **Docker & Docker Compose**(Necessary for the development environment)
- **Java JDK 17, Maven & an IDE**(Optional. Necessary for developing)
- **AWS Account**(Optional. Necessary for UAT/production environment)

## Development Environment (Running with Docker Compose)
<img src="files/development.png" width="100%" height="100%">

Docker Compose orchestrates a deployment environment that, while distinct from production environment, offers a comparable setup.

- [Spins](docker-compose.yml?ref_type=heads#L3) an instance of MYSQL
- [Spins](docker-compose.yml?ref_type=heads#L22) an instance of NGINX on port 3050(To simulate ELB on AWS)
- [Builds](docker-compose.yml?ref_type=heads#L10) recipeservice SpringBoot application
- [Runs](recipeservice/Dockerfile.dev?ref_type=heads#L8) unit and integration tests
- [Runs](recipeservice/Dockerfile.dev?ref_type=heads#L10) recipeservice SpringBoot application

One docker-compose command brings up the whole development infrastructure.

```bash
git clone https://gitlab.com/ooziesoft/recipeservice.git
cd recipeservice
docker-compose up [--build]
```

###### Usage
 
Test Users(Username/Password): *dev1/dev1, dev2/dev2, uat1/uat1, uat2/uat2*

Resource URL:

```
http://localhost:3050/api/recipe
```

Swagger UI:

```
http://localhost:3050/swagger-ui/index.html
```


## Local Environment (Running on IDE)

<img src="files/localenvironment.png" >

If you run the application through your IDE or directly, recipeservice SpringBoot application runs with H2 in-memory database.

This time, the application will run on a well-known port(8080) as there is no NGINX.

Run the application within your IDE or run:

```bash
java -jar recipeservice.jar
```

To create test users, use the LOCAL profile:

```bash
 -Dspring.profiles.active=DEV
```

###### Usage

Resource URL:

```
http://localhost:8080/api/recipe
```

Swagger UI:

```
http://localhost:8080/swagger-ui/index.html
```


## Configuration

The application uses dynamic configuration capability, powered by Spring. [Out of the box](recipeservice/recipeservice-backend/src/main/resources/application.yml?ref_type=heads), the default local configuration is as follows:

```yml
env: DEFAULT
spring:
#adding manual datasource configuration to access via h2-console
   datasource:
      driver-class-name: org.h2.Driver
      url: jdbc:h2:mem:db
      username: sa
      password: sa
   jpa:
      hibernate.format_sql: true
      hibernate.jdbc.time_zone: UTC
      ddl-auto: create
      show-sql: true
   h2:
      console:
         enabled: true
         path: /h2-console
   flyway:
      enabled: false
app:
   version: 1.0.6
management:
   endpoints.web.exposure.include: env
```

On the development and UAT/production environments, [specific properties](docker-compose.yml?ref_type=heads#L23), those related to database configurations, are dynamically overridden by the container environments. On UAT/production environment, passwords are [secretly handled](.gitlab-ci.yml?ref_type=heads#L22) during CI/CD and they are kept in variable store of Gitlab.

## Logging

When running locally on your machine, logs are directed to the console, providing immediate and straightforward access for monitoring and debugging. On UAT/production environment logs are monitored through AWS CloudWatch.

Log4J2 framework is [pre-configured](recipeservice/recipeservice-backend/src/main/resources/log4j2.xml).

If you wish to tailor the logging behavior to better suit your specific requirements, you can easily override the default configuration. One way is specifying a configuration file when launching the SpringBoot application:

```bash
-Dlog4j.configurationFile=path/to/your/log4j2.xml
```

## Spring Profiles

To adapt to various environments, I use Spring profiles. Currently, I use four profiles:

- **No Profile**: By default, when no specific profile is activated, the application loads the standard configuration.
- **LOCAL**: Aimed at running at localhost. Test users are created.
- **DEV**: Aimed at the development environment. Test users are created. Environment variables related to database configurations are dynamically overridden by the container environment. 
- **UAT**: Aimed at the UAT environment. Test users are created. Certain Hibernate configurations pertaining to automatic DDL generation have been adjusted. Environment variables related to database configurations are dynamically overridden by the container environment. 
- **LIVE**: Aimed at the production environment. Certain Hibernate configurations pertaining to automatic DDL generation have been adjusted. Environment variables related to database configurations are dynamically overridden by the container environment. 

## Code Quality and Documentation(JavaDocs)

The project's build phase includes static code analysis, coverage checks, and JavaDoc generation, using specific Maven plugins:

- **[PMD](recipeservice/pom.xml?ref_type=heads#L156)**: This plugin checks coding standards and style guidelines in Java projects by analyzing source code during the build process.
- **[SpotBugs](recipeservice/pom.xml?ref_type=heads#L169)**: This plugin checks coding standards and style guidelines in Java projects by analyzing source code during the build process.
- **[Check Style](recipeservice/pom.xml?ref_type=heads#L186)**: This plugin checks coding standards and style guidelines in Java projects by analyzing source code during the build process.
- **[Jococo](recipeservice/pom.xml?ref_type=heads#L205)**: The JaCoCo Maven Plugin is a widely-used tool for analyzing code coverage.

You can access the project report and the documentation from this [link](https://ooziesoft.gitlab.io/recipeservice/public/recipeservice-backend/index.html).

## Data Model

Recipe:

| Attribute       | Type          | Description                                      |
|-----------------|---------------|--------------------------------------------------|
| id              | String        | The unique identifier for this entity.           |
| createdAt       | Long          | The creation date in Unix epoch time.            |
| lastUpdate      | Long          | The last modification date in Unix epoch time.   |
| username        | String        | The owner of the recipe.                         |
| name            | String        | The name of the recipe.                          |
| vegetarian      | Boolean       | Indicates if the recipe is vegetarian.           |
| servings        | Integer       | Number of servings.                              |
| ingredients     | JSON String   | JSON representation of ingredients.              |
| instructions    | String        | Cooking instructions for the recipe.             |

indexes: vegetarian, servings, instructions(FullText), ingredients(FullText)

Ingredients:

| Attribute  | Type     | Description                  |
|------------|----------|------------------------------|
| name       | String   | Name of the item.            |
| quantity   | String   | Quantity of the item.        |
| unit       | String   | Unit of measurement for item.|

Users:

| Attribute  | Type     | Description                   |
|------------|----------|-------------------------------|
| username   | String   | User's username.              |
| password   | String   | User's password.              |
| role       | String   | User's role or authorization. |


The recipe model has been denormalized to enhance access speed and create a more adaptable schema suitable for use in NoSQL databases like MongoDB.
Physical data model is managed by [flyway](recipeservice/recipeservice-backend/src/main/resources/application-LIVE.yml#L6) on DEV/UAT/LIVE profiles.

## Issues and future plans

Please check the [issue board](https://gitlab.com/ooziesoft/recipeservice/-/issues).

